<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// 무통장입금 주문 상품삭제
function order_item_clean()
{
    global $g5,$member;

    // 주문 보관일
    $keep_term = 2;

    $statustime = G5_SERVER_TIME - (86400 * $keep_term);

    $sql = "select * from {$g5['g5_shop_order_table']} where od_status = '주문' and UNIX_TIMESTAMP(od_time) < '$statustime'";
    $result = sql_query($sql);

    for($i=0;$row=sql_fetch_array($result);$i++){

        if($row['od_status'] != '주문') continue;

        $data = serialize($row);

        $sql = " insert {$g5['g5_shop_order_delete_table']} set de_key = '{$row['od_id']}', de_data = '".addslashes($data)."', mb_id = '{$member['mb_id']}', de_ip = '{$_SERVER['REMOTE_ADDR']}', de_datetime = '".G5_TIME_YMDHIS."' ";
        sql_query($sql, true);

        // cart 테이블의 상품 상태를 삭제로 변경
        $sql = " update {$g5['g5_shop_cart_table']} set ct_status = '삭제' where od_id = '{$row['od_id']}' and ct_status = '주문' ";
        sql_query($sql);

        $sql = " delete from {$g5['g5_shop_order_table']} where od_id = '{$row['od_id']}' ";
        sql_query($sql);

    }

}
?>