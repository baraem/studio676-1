<?php
include_once('./_common.php');

if (!$is_member)
    goto_url(G5_BBS_URL."/login.php?url=".urlencode(G5_SHOP_URL."/mypage.php"));

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/mypage.php');
    return;
}

// 테마에 mypage.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_mypage_file = G5_THEME_SHOP_PATH.'/mypage.php';
    if(is_file($theme_mypage_file)) {
        include_once($theme_mypage_file);
        return;
        unset($theme_mypage_file);
    }
}

$g5['title'] = '마이페이지';
include_once('./_head.php');

// 쿠폰
$cp_count = 0;
$sql = " select cp_id
            from {$g5['g5_shop_coupon_table']}
            where mb_id IN ( '{$member['mb_id']}', '전체회원' )
              and cp_start <= '".G5_TIME_YMD."'
              and cp_end >= '".G5_TIME_YMD."' ";
$res = sql_query($sql);

for($k=0; $cp=sql_fetch_array($res); $k++) {
    if(!is_used_coupon($member['mb_id'], $cp['cp_id']))
        $cp_count++;
}
?>

<!--마이페이지 시작-->
<div class="mypage_con">
    <div class="title">
        <h2>MYPAGE</h2>
    </div>

    <!--회원 기본정보-->
    <div class="mypage_memData">

        <!--개인정보 수정-->
        <div class="memData1">
            <?=$member['mb_name']?>님
            <div class="memData1_btn_con">
                <a href="<?php echo G5_BBS_URL; ?>/member_confirm.php?url=register_form.php" class="btn01">정보수정</a>
                <a href="<?php echo G5_BBS_URL; ?>/member_confirm.php?url=member_leave.php" onclick="return member_leave();" class="btn01">회원탈퇴</a>
            </div>
        </div>
        
        <!--보유 포인트/쿠폰-->
        <div class="memData2">
            <div class="memData2_1">
                <p class="memData2_1_p1">보유쿠폰</p>
                <p class="memData2_1_p2"><a href="<?php echo G5_SHOP_URL; ?>/coupon.php" target="_blank" class="win_coupon"><?php echo number_format($cp_count); ?></a></p>
            </div>

            <div class="memData2_2">
                <p class="memData2_2_p1">보유포인트</p>
                <p class="memData2_2_p2"><a href="<?php echo G5_BBS_URL; ?>/point.php" target="_blank" class="win_point"><?php echo number_format($member['mb_point']); ?>점</p>
            </div>
        </div>
    </div>

    <!--마이페이지 각종 버튼들-->
    <div class="member_btn_con">
        <a href="<?php echo G5_BBS_URL; ?>/member_confirm.php?url=register_form.php">
            <div class="member_btn">
                <span class="icon_span"><img src="<?=G5_THEME_URL?>/img/icon/icon1.png" alt="icon" /></span>
                <p class="member_btn_title">회원정보</p>
                <p class="member_btn_right">></p>
            </div>
        </a>

        <a href="./orderinquiry.php">
            <div class="member_btn">
                <span class="icon_span"><img src="<?=G5_THEME_URL?>/img/icon/icon2.png" alt="icon" /></span>
                <p class="member_btn_title">주문내역</p>
                <p class="member_btn_right">></p>
            </div>
        </a>

        <a href="./wishlist.php">
            <div class="member_btn">
                <span class="icon_span"><img src="<?=G5_THEME_URL?>/img/icon/icon3.png" alt="icon" /></span>
                <p class="member_btn_title">위시리스트</p>
                <p class="member_btn_right">></p>
            </div>
        </a>

        <a href="<?=G5_BBS_URL?>/board.php?bo_table=qa">
            <div class="member_btn">
                <span class="icon_span"><img src="<?=G5_THEME_URL?>/img/icon/icon4.png" alt="icon" /></span>
                <p class="member_btn_title">1:1문의</p>
                <p class="member_btn_right">></p>
            </div>
        </a>


        <a href="<?=G5_SHOP_URL?>/personalpay.php">
            <div class="member_btn">
                <span class="icon_span"><img src="<?=G5_THEME_URL?>/img/icon/icon5.png" alt="icon" /></span>
                <p class="member_btn_title">개인결제</p>
                <p class="member_btn_right">></p>
            </div>
        </a>
    </div>
</div>

<script>
$(function() {
    $(".win_coupon").click(function() {
        var new_win = window.open($(this).attr("href"), "win_coupon", "left=100,top=100,width=700, height=600, scrollbars=1");
        new_win.focus();
        return false;
    });
});

function member_leave()
{
    return confirm('정말 회원에서 탈퇴 하시겠습니까?')
}
</script>
<!-- } 마이페이지 끝 -->

<?php
include_once("./_tail.php");
?>