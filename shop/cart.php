<?php
include_once('./_common.php');
include_once(G5_SHOP_PATH.'/settle_naverpay.inc.php');

// 보관기간이 지난 상품 삭제
cart_item_clean();

// cart id 설정
set_cart_id($sw_direct);

$s_cart_id = get_session('ss_cart_id');
// 선택필드 초기화
$sql = " update {$g5['g5_shop_cart_table']} set ct_select = '0' where od_id = '$s_cart_id' ";
sql_query($sql);

$cart_action_url = G5_SHOP_URL.'/cartupdate.php';

if (G5_IS_MOBILE) {
    include_once(G5_MSHOP_PATH.'/cart.php');
    return;
}

// 테마에 cart.php 있으면 include
if(defined('G5_THEME_SHOP_PATH')) {
    $theme_cart_file = G5_THEME_SHOP_PATH.'/cart.php';
    if(is_file($theme_cart_file)) {
        include_once($theme_cart_file);
        return;
        unset($theme_cart_file);
    }
}

$g5['title'] = '장바구니';
include_once('./_head.php');

?>

<?
    if($_SERVER['SCRIPT_URL'] == '/shop/cart.php')
    {?>
        <style>
            #container{
                background-color: #f5f5f5;
                margin-bottom: 60px;
                height: 100% !important;
                padding: 10px 10px 0 10px;
            }
            header {
                height: 130px;
            }
        </style>
    <?}
?>
<!-- 장바구니 시작 { -->
<script src="<?php echo G5_JS_URL; ?>/shop.js"></script>
<script src="<?php echo G5_JS_URL; ?>/shop.override.js"></script>

<div class="cart_con">
    <div class="cart_title"><?=$g5['title']?></div>

    <!--장바구니 리스트 시작-->
    <div class="cart_list_con">
        <div class="cart_list_chkbox_con">
            <input type="checkbox" name="ct_all" value="1" id="ct_all" checked="checked">
            <label for="ct_all" class="">전체상품 선택</label>
        </div>

        <div class="cart_list_">
            <form name="frmcartlist" id="sod_bsk_list" class="2017_renewal_itemform" method="post" action="<?php echo $cart_action_url; ?>">
                <?php
                $tot_point = 0;
                $tot_sell_price = 0;

                // $s_cart_id 로 현재 장바구니 자료 쿼리
                $sql = " select a.ct_id,
                        a.it_id,
                        a.it_name,
                        a.ct_price,
                        a.ct_point,
                        a.ct_qty,
                        a.ct_status,
                        a.ct_send_cost,
                        a.it_sc_type,
                        b.ca_id,
                        b.ca_id2,
                        b.ca_id3
                   from {$g5['g5_shop_cart_table']} a left join {$g5['g5_shop_item_table']} b on ( a.it_id = b.it_id )
                  where a.od_id = '$s_cart_id' ";
                $sql .= " group by a.it_id ";
                $sql .= " order by a.ct_id ";
                $result = sql_query($sql);

                $it_send_cost = 0;

                for ($i=0; $row=sql_fetch_array($result); $i++)
                {
                // 합계금액 계산
                $sql = " select SUM(IF(io_type = 1, (io_price * ct_qty), ((ct_price + io_price) * ct_qty))) as price,
                            SUM(ct_point * ct_qty) as point,
                            SUM(ct_qty) as qty
                        from {$g5['g5_shop_cart_table']}
                        where it_id = '{$row['it_id']}'
                          and od_id = '$s_cart_id' ";
                $sum = sql_fetch($sql);

                if ($i==0) { // 계속쇼핑
                    $continue_ca_id = $row['ca_id'];
                }

                $a1 = '<a href="./item.php?it_id='.$row['it_id'].'" class="prd_name"><b>';
                $a2 = '</b></a>';
                $image = get_it_image($row['it_id'], 80, 80);

                $it_name = $a1 . stripslashes($row['it_name']) . $a2;
                $it_options = print_item_options($row['it_id'], $s_cart_id);

                if($it_options) {
                    $mod_options = '<div class="sod_option_btn"><button type="button" class="mod_options">선택사항수정</button></div>';
                    $it_name .= '<div class="sod_opt">'.$it_options.'</div>';
                }

                // 배송비
                switch($row['ct_send_cost'])
                {
                    case 1:
                        $ct_send_cost = '착불';
                        break;
                    case 2:
                        $ct_send_cost = '무료';
                        break;
                    default:
                        $ct_send_cost = '선불';
                        break;
                }

                // 조건부무료
                if($row['it_sc_type'] == 2) {
                    $sendcost = get_item_sendcost($row['it_id'], $sum['price'], $sum['qty'], $s_cart_id);

                    if($sendcost == 0)
                        $ct_send_cost = '무료';
                }

                $point      = $sum['point'];
                $sell_price = $sum['price'];

                // 품절여부
                    $product_soldout_query = "SELECT it_id, ca_id, it_name, it_soldout FROM BRI_SHOP_item WHERE it_id = '{$row['it_id']}' ";
                    $product_soldout_result = sql_fetch($product_soldout_query);
                    //print_r($product_soldout_result);
                ?>

                <!--상품 이름, 체크박스-->
                <div class="cart_list_title_chk">
                    <label for="ct_chk_<?php echo $i; ?>" class="sound_only">상품</label>
                    <input type="checkbox" name="ct_chk[<?php echo $i; ?>]" value="1" id="ct_chk_<?php echo $i; ?>" <?=$product_soldout_result['it_soldout'] == 0 ? 'checked="checked"' : 'readonly disabled'?> >
                    <div class="cart_list_title">
                        <input type="hidden" name="it_id[<?php echo $i; ?>]"    value="<?php echo $row['it_id']; ?>">
                        <input type="hidden" name="it_name[<?php echo $i; ?>]"  value="<?php echo get_text($row['it_name']); ?>">
                        <?=$row['it_name']?>
                        <span style="color:red"><?=$product_soldout_result['it_soldout'] == 1 ? ' (품절)' : ''?></span>
                    </div>
                </div>

                <!--제품 정보-->
                <div class="cart_list_item">
                    
                    <!--제품 이미지-->
                    <div class="cart_list_conteent cart_list_img">
                        <?=$image;?>
                    </div>
                    
                    <!--옵션-->
                    <div class="cart_list_conteent cart_list_option">
                        <?=$it_options;?>
                        <?=$mod_options;?>
                    </div>
                    <div class="cart_list_line"></div>
                </div>

                <!--제품 정보2-->
                <div class="cart_list_item_con2">
                    <!--판매가, 배송비-->
                    <div class="cart_list_item_con2_1">
                        <div class="cart_list_item_price">
                            <p class="cart_list_item_p1">판매가</p>
                            <p class="cart_list_item_p2"><?=number_format($row['ct_price']);?></p>
                        </div>

                        <div class="cart_list_item_send_cost">
                            <p class="cart_list_item_p1">배송비</p>
                            <p class="cart_list_item_p2"><?=$ct_send_cost;?></p>
                        </div>
                    </div>



                    <!--수량, 적립 포인트-->
                    <div class="cart_list_item_con2_2">
                        <div class="cart_list_item_qty">
                            <p class="cart_list_item_p1">수량</p>
                            <p class="cart_list_item_p2"><?=number_format($sum['qty']);?></p>
                        </div>

                        <div class="cart_list_item_point">
                            <p class="cart_list_item_p1">포인트</p>
                            <p class="cart_list_item_p2"><?=number_format($point);?></p>
                        </div>
                    </div>
                </div>
                
                <!--소계-->
                <div class="cart_list_item_smll_count">
                    <div class="cart_list_item_sell">
                        <p class="cart_list_item_p3">소계</p>
                        <p class="cart_list_item_p4"><?=number_format($sell_price);?></p>
                    </div>
                </div>

                <?php
                    $tot_point      += $point;
                    $tot_sell_price += $sell_price;
                } // for 끝

                if ($i == 0) {
                    echo '<tr><td colspan="8" class="empty_table">장바구니에 담긴 상품이 없습니다.</td></tr>';
                } else {
                    // 배송비 계산
                    $send_cost = get_sendcost($s_cart_id, 0);
                }
                ?>

                <div class="btn_cart_del">
                    <button type="button" onclick="return form_check('seldelete');">선택삭제</button>
                    <button type="button" onclick="return form_check('alldelete');">비우기</button>
                </div>

                <!--가격표-->
                <div class="cart_list_btn_con">
                    <?php
                    $tot_price = $tot_sell_price + $send_cost; // 총계 = 주문상품금액합계 + 배송비
                    if ($tot_price > 0 || $send_cost > 0) {
                        ?>
                        <div id="sod_bsk_tot" class="cart_list_total">
                            <div class="cart_list_send">
                                <p class="cart_list_item_p5">배송비</p>
                                <p class="cart_list_item_p6"><?php echo number_format($send_cost); ?>점</p>
                            </div>

                            <div class="cart_list_point">
                                <p class="cart_list_item_p5">포인트</p>
                                <p class="cart_list_item_p6"><?php echo number_format($tot_point); ?>점</p>
                            </div>

                            <div class="cart_list_price">
                                <p class="cart_list_item_p5">총계</p>
                                <p class="cart_list_item_p6" style="color: #ff006c"><?php echo number_format($tot_price); ?>점</p>
                            </div>

                        </div>
                    <?php } ?>

                    <!---->
                    <div id="sod_bsk_act">
                        <?php if ($i == 0) { ?>
                            <a href="" onclick="returns();return false" class="btn01">쇼핑 계속하기</a>
                        <?php } else { ?>
                            <input type="hidden" name="url" value="./orderform.php">
                            <input type="hidden" name="records" value="<?php echo $i; ?>">
                            <input type="hidden" name="act" value="">
                            <a href="<?php echo G5_SHOP_URL; ?>/list.php?ca_id=<?php echo $continue_ca_id; ?>" class="btn01">쇼핑 계속하기</a>
                            <button type="button" onclick="return form_check('buy');" class="btn_submit"><i class="fa fa-credit-card" aria-hidden="true"></i> 주문하기</button>

                            <?php if ($naverpay_button_js) { ?>
                                <div class="cart-naverpay"><?php echo $naverpay_request_js.$naverpay_button_js; ?></div>
                            <?php } ?>
                        <?php } ?>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(function() {
        var close_btn_idx;

        // 선택사항수정
        $(".mod_options").click(function() {
            var it_id = $(this).parent().parent().parent().parent().find('div').eq(0).find('div').find("input[name^=it_id]").val();
            var $this = $(this);
            close_btn_idx = $(".mod_options").index($(this));

            $.post(
                "./cartoption.php",
                { it_id: it_id },
                function(data) {
                    $("#mod_option_frm").remove();
                    $this.after("<div id=\"mod_option_frm\"></div>");
                    $("#mod_option_frm").html(data);
                    price_calculate();
                }
            );
        });


    // 모두선택
    $("input[name=ct_all]").click(function() {
        if($(this).is(":checked"))
        {
            $("input[name^=ct_chk]").attr("checked", true);
            $.each($("input[name^=ct_chk]"), function(k, v){
                if( $("input[name^=ct_chk]").eq(k).is(':disabled') )
                    $("input[name^=ct_chk]").eq(k).attr("checked", false);
            })
        }
        else
        {
            $("input[name^=ct_chk]").attr("checked", false);
        }
    });

    // 옵션수정 닫기
    $(document).on("click", "#mod_option_close", function() {
        $("#mod_option_frm").remove();
        $(".mod_options").eq(close_btn_idx).focus();
    });
    $("#win_mask").click(function () {
        $("#mod_option_frm").remove();
        $(".mod_options").eq(close_btn_idx).focus();
    });

});

function fsubmit_check(f) {
    if($("input[name^=ct_chk]:checked").size() < 1) {
        alert("구매하실 상품을 하나이상 선택해 주십시오.");
        return false;
    }

    return true;
}

function form_check(act) {
    var f = document.frmcartlist;
    var cnt = f.records.value;

    if (act == "buy")
    {
        if($("input[name^=ct_chk]:checked").size() < 1) {
            alert("주문하실 상품을 하나이상 선택해 주십시오.");
            return false;
        }

        f.act.value = act;
        f.submit();
    }
    else if (act == "alldelete")
    {
        f.act.value = act;
        f.submit();
    }
    else if (act == "seldelete")
    {
        if($("input[name^=ct_chk]:checked").size() < 1) {
            alert("삭제하실 상품을 하나이상 선택해 주십시오.");
            return false;
        }

        f.act.value = act;
        f.submit();
    }

    return true;
}

</script>
<!-- } 장바구니 끝 -->

<?

$item = preg_match('/list.php/i', $_SERVER['HTTP_REFERER']);
$item2 = preg_match('/item.php/i', $_SERVER['HTTP_REFERER']);

?>
<script>
    function returns(){
        var item = '<?=$item?>';
        var item2 = '<?=$item2?>';

        if( item == 1 )
        {
            history.go(-1);
        }
        else if( item2 == 1 )
        {
            history.go(-2);
        }
        else
        {
            location.href='<?=G5_SHOP_URL?>/list.php?ca_id=10';
        }
    }
</script>
<?php
include_once('./_tail.php');
?>