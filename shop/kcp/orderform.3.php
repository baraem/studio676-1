<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가
?>

<style>
    input[type=button]{
        -webkit-appearance: none;
        -moz-appearance: none;
        -webkit-border-radius: 0;
    }

    .btn_confirm{
        width: 100%;
    }
</style>

<div id="display_pay_button" class="btn_confirm">
    <input type="button" value="주문하기" onclick="forderform_check(this.form);" class="btn_submit">
    <a href="javascript:history.go(-1);" class="btn01">취소</a>
</div>
<div id="display_pay_process" style="display:none">
    <img src="<?php echo G5_URL; ?>/shop/img/loading.gif" alt="">
    <span>주문완료 중입니다. 잠시만 기다려 주십시오.</span>
</div>