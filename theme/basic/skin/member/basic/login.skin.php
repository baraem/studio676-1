<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);
?>
<style>
    #login_main_div1{
        width: 100%;
        background-color: #ffffff;
    }
    .login_main_h1{
        font-size: 15px;
        font-family: 나눔바른고딕;
        font-weight: bold;
    }

    .login_main_div2{
        width: 980px;
        margin:0 auto;
    }
    .login_main_div3{
        border: 1px solid #bbb;
        width: 980px;
        height: 183px;
        display: inline-block;
    }
    .login_text1{
        font-size: 14px;
        font-family: 나눔바른고딕;
    }

    .login_input_text{
        width: 243px;
        height: 33px;
        border: 1px solid #c2c2c2;
        margin-left: 33px;
        padding-left: 15px;
    }
    .login_btn_sub1{
        font-size: 16px;
        font-family: 나눔바른고딕;
        font-weight: bold;
        padding: 34px 25px 32px 25px;
        border: none;
        background-color: #363636;
        color: #ffffff;
        margin-bottom: 46px;
        margin-left: 5px;
    }
    .login_sub_div1{
        display: inline-block;
        width: 440px;
        height: 182px;
        border-left: 1px solid #c2c2c2;
        vertical-align: top;
        margin-left: -11px;
        padding: 40px 0px 0px 32px;

    }
    .login_sub_div2{
        width: 366px;
        display: inline-block;
    }

    .login_text2{
        font-size: 14px;
        font-family: 나눔바른고딕;
        font-weight: bold;
    }
    .login_text3{
        font-size: 14px;
        font-family: 나눔바른고딕;
    }

    .login_btn_btn1{
        width: 123px;
        height: 31px;
        font-size: 14px;
        font-family: 나눔바른고딕;
        text-decoration: none;
        font-weight: bold;
        color: #ffffff;
        background-color: #acacac;
        border: none;
        text-align: center;
        padding-top: 6px;
        float: right;
        margin-left: 60px;

    }

    .login_main_div4{
        margin-top: 30px;
    }

    .login_sub_div3{
        margin-top: 10px;
        border-top: 1px solid #363636;
        padding-top: 20px;
    }
    .login_btn_a{
        border: none;
        text-decoration: none;
        float: right;
        width: 123px;
        height: 37px;
        text-align: center;
        padding-top: 10px;
        background-color: #363636;
        color: #ffffff;
    }
</style>
<!-- 로그인 시작 { -->
<div id="login_main_div1">

    <div class="login_main_div2">
    <h1 class="login_main_h1"><?php echo $g5['title'] ?></h1>

    <div class="login_main_div3">
        <form name="flogin" action="<?php echo $login_action_url ?>" onsubmit="return flogin_submit(this);" method="post" style="width: 530px;display: inline-block">
            <input type="hidden" name="url" value="<?php echo $login_url ?>">

            <fieldset id="login_fs">
                <div style="display: inline-block">
                    <div style="margin-top: 40px;">
                        <label for="login_id" class="login_text1" >아이디&nbsp;&nbsp;&nbsp;</label>
                        <input type="text" name="mb_id" id="login_id" required class="login_input_text">
                    </div>
                    <div style="margin-top: 15px;">
                        <label for="login_pw" class="login_text1">비밀번호</label>
                        <input type="password" name="mb_password" id="login_pw" required class="login_input_text">
                    </div>
                </div>
                <div style="display: inline-block">
                    <input type="submit" value="Login" class="login_btn_sub1">
                </div>
            </fieldset>
        </form>
        <div class="login_sub_div1">
                <div class="login_sub_div2" style="margin-bottom: 21px">
                    <div style="display: inline-block;">
                        <p class="login_text2">FIND ID</p>
                        <p class="login_text3">아이디가 기억나지 않으세요?&nbsp;&nbsp;&nbsp;</p>
                    </div>
                    <div style="display: inline-block;">
                        <a href="./id_lost.skin.php" class="login_btn_btn1">아이디 찾기</a>
                    </div>
                </div>
                <div class="login_sub_div2">
                    <div style="display: inline-block;" >
                        <p class="login_text2">FIND PASSWORD</p>
                        <p class="login_text3">비밀번호가 기억나지 않으세요?</p>
                    </div>
                    <div style="display: inline-block; text-align: right">
                        <a href="<?php echo G5_BBS_URL ?>/password_lost.php" class="login_btn_btn1">비밀번호 찾기</a>
                    </div>
                </div>
        </div>
    </div>

    <div class="login_main_div4">
        <h1 class="login_main_h1">JOIN</h1>
        <div class="login_sub_div3">
            <p style="font-size: 14px; font-family: 나눔바른고딕; display: inline-block">수입차부품마트 회원이 되시면 다양한 서비스를 받아보실 수 있습니다</p>
            <a href="./register.php" class="login_btn_a">회원가입</a>
        </div>
    </div>
</div>

    <?php // 쇼핑몰 사용시 여기부터 ?>
    <?php if ($default['de_level_sell'] == 1) { // 상품구입 권한 ?>

        <!-- 주문하기, 신청하기 -->
        <?php if (preg_match("/orderform.php/", $url)) { ?>

            <section id="mb_login_notmb" class="mbskin">
                <h2>비회원 구매</h2>

                <p>
                    비회원으로 주문하시는 경우 포인트는 지급하지 않습니다.
                </p>

                <div id="guest_privacy">
                    <?php echo $default['de_guest_privacy']; ?>
                </div>

                <label for="agree">개인정보수집에 대한 내용을 읽었으며 이에 동의합니다.</label>
                <input type="checkbox" id="agree" value="1">

                <div class="btn_confirm">
                    <a href="javascript:guest_submit(document.flogin);" class="btn_submit">비회원으로 구매하기</a>
                </div>

                <script>
                    function guest_submit(f)
                    {
                        if (document.getElementById('agree')) {
                            if (!document.getElementById('agree').checked) {
                                alert("개인정보수집에 대한 내용을 읽고 이에 동의하셔야 합니다.");
                                return;
                            }
                        }

                        f.url.value = "<?php echo $url; ?>";
                        f.action = "<?php echo $url; ?>";
                        f.submit();
                    }
                </script>
            </section>

        <?php } else if (preg_match("/orderinquiry.php$/", $url)) { ?>
            <div class="mbskin" id="mb_login_od_wr">
                <h2>비회원 주문조회 </h2>

                <fieldset id="mb_login_od">
                    <legend>비회원 주문조회</legend>

                    <form name="forderinquiry" method="post" action="<?php echo urldecode($url); ?>" autocomplete="off">

                        <label for="od_id" class="od_id sound_only">주문서번호<strong class="sound_only"> 필수</strong></label>
                        <input type="text" name="od_id" value="<?php echo $od_id; ?>" id="od_id" required class="frm_input required" size="20" placeholder="주문서번호">
                        <label for="id_pwd" class="od_pwd sound_only" >비밀번호<strong class="sound_only"> 필수</strong></label>
                        <input type="password" name="od_pwd" size="20" id="od_pwd" required class="frm_input required" placeholder="비밀번호">
                        <input type="submit" value="확인" class="btn_submit">

                    </form>
                </fieldset>

                <section id="mb_login_odinfo">
                    <p>메일로 발송해드린 주문서의 <strong>주문번호</strong> 및 주문 시 입력하신 <strong>비밀번호</strong>를 정확히 입력해주십시오.</p>
                </section>

            </div>
        <?php } ?>

    <?php } ?>
    <?php // 쇼핑몰 사용시 여기까지 반드시 복사해 넣으세요 ?>

    </form>

</div>

<!-- } 로그인 끝 -->
