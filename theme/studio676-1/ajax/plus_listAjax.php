<?php
/**
 * Created by PhpStorm.
 * User: BARAEM
 * Date: 2018-08-21
 * Time: 오후 6:33
 */

include_once ($_SERVER['DOCUMENT_ROOT'].'/common.php');

$ca_id = $_POST[0]; //품목 번호
$page  = ((int)$_POST[1]) * 8 -8; //리미트값
$order_1 = $_POST[2]; //정렬조건1
$order_2 = $_POST[3]; //정렬조건2

$sql = "
    SELECT
        *
     FROM
        BRI_SHOP_item
";

$sql .= " WHERE ca_id LIKE '{$ca_id}%' or ca_id2 LIKE '{$ca_id}%'";

//if( strlen($ca_id) < 3  )
//{
//
//}
//else
//{
//    $sql .= " WHERE ca_id2 = '{$ca_id}'";
//}


//정렬조건이 있다면
if( $order_1 )
{
    switch ( $order_1 )
    {
        //가격순
        case 'it_price' :
            //가격 낮은순
            if( $order_2 == 'asc' )
            {
                $sql .= " ORDER BY it_price ASC";
            }
            else if( $order_2 == 'desc' )
            {
                $sql .= " ORDER BY it_price DESC";
            }
            break;
        //최신등록순
        case 'it_update_time' :
            $sql .= " ORDER BY it_update_time DESC";
            break;
        //판매 많은순
        case 'it_sum_qty' :
            $sql .= " ORDER BY it_sum_qty DESC";
            break;
        default:
            $sql .= " ORDER BY it_id DESC";
            break;
    }
}
else
{
    $sql .= " ORDER BY it_order, it_id DESC";
}

$sql .= " LIMIT {$page}, 8";

$result = sql_query($sql);
while($row = $result -> fetch_array())
{
    $list[] = $row;
}

echo json_encode($list);