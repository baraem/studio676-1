<script>
    //$(document).ready(function () {
    //    $.ajax ({
    //        type : "GET",                     // GET 또는 POST
    //        url : '<?//=G5_THEME_URL?>///api/instagram/instagram.php',          // 서버측에서 가져올 페이지
    //        dataType : 'html',               // html , javascript, text, xml, jsonp 등이 있다
    //        success : function(data) {     // 정상적으로 완료되었을 경우에 실행된다
    //            if(data){
    //                $("#instagram_api").append(data);
    //            }
    //        },
    //        error : function(request, status, error ) {   // 오류가 발생했을 때 호출된다.
    //
    //            console.log("인스타그램 api호출 오류");
    //
    //        }
    //
    //    });
    //});
</script>


<div id="instagram_api" class="instagram_api">
    <?php
    $sql = "SELECT * FROM `BRI_instagram`";
    $result = sql_query($sql);

    if ($result) { ?>
    <p class="p_01">INSTAGRAM @ <span class="span_01"> mikyeong_kang</span></p>
    <ul id="instafeed">
        <?php
        for ($i = 0; $row = sql_fetch_array($result); $i++) {
            echo "<li>";
            echo "<a href='" . $row['ig_link'] . "' target='_blank'><img src='" . $row['ig_img'] . "' alt='인스타피드'></a>";
            echo "</li>";
        }
        }
        ?>
    </ul>
</div>


<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

if (G5_IS_MOBILE) {
    include_once(G5_THEME_MSHOP_PATH . '/shop.tail.php');
    return;
}

$admin = get_admin("super");

// 사용자 화면 우측과 하단을 담당하는 페이지입니다.
// 우측, 하단 화면을 꾸미려면 이 파일을 수정합니다.
?>

</div>
<!-- } 콘텐츠 끝 -->

<!-- 하단 시작 { -->
</div>
<div id="footer">
    <div class="quick">
        <ul>
            <li><a href="/board/board.php?bo_table=notice">
                    <p class="img"><img src="<?= G5_THEME_URL ?>/img/f_ico_01.jpg" alt="NOTICE"/></p>
                    <p class="txt"> NOTICE</p></a></li>
            <li><a href="/board/board.php?bo_table=qa"><p class="img"><img src="<?= G5_THEME_URL ?>/img/f_ico_02.jpg" alt="Q&A"/></p>
                    <p class="txt"> Q&A</p></a></li>
            <li><a href="/board/board.php?bo_table=review"><p class="img"><img
                                src="<?= G5_THEME_URL ?>/img/f_ico_03.jpg" alt="REVIEW"/></p>
                    <p class="txt"> REVIEW</p></a></li>
            <li><a href="https://www.cjlogistics.com/ko/tool/parcel/tracking" target="_blank"><p class="img"><img
                                src="<?= G5_THEME_URL ?>/img/f_ico_04.jpg" alt="DELIVERY"/></p>
                    <p class="txt"> DELIVERY</p></a></li>
            <li><a href="<?= G5_SHOP_URL ?>/listtype.php?type=4"><p class="img"><img
                                src="<?= G5_THEME_URL ?>/img/f_ico_05.jpg" alt="BEST ITEM"/></p>
                    <p class="txt"> BEST ITEM</p></a></li>
            <li><a href="<?= G5_SHOP_URL ?>/listtype.php?type=3"><p class="img"><img
                                src="<?= G5_THEME_URL ?>/img/f_ico_06.jpg" alt="NEW"/></p>
                    <p class="txt"> NEW</p></a></li>
            <li><a href="https://www.instagram.com/mikyeong_kang/" target="_blank"><p class="img"><img
                                src="<?= G5_THEME_URL ?>/img/f_ico_07.jpg" alt="INSTAGRAM"/></p>
                    <p class="txt"> INSTAGRAM</p></a></li>
            <li><a href="http://676-1studio.co.kr" target="_blank"><p class="img"><img
                                src="<?= G5_THEME_URL ?>/img/f_ico_08.jpg" alt="BLOG"/></p>
                    <p class="txt">MARKET</p></a></li>
        </ul>
    </div>
    <div class="cs">
        <div class="box">
            <p class="tlt">
                CS CENTER
            </p>
            <p class="num">
                010-3696-5305
            </p>
            <p class="txt">
                평일 10시 - 6시 토,일,공휴일 휴무
            </p>
        </div>
        <div class="center_line"></div>
        <div class="box">
            <p class="tlt">
                Bank info
            </p>
            <p class="num">
                경남은행 01036965305
            </p>
            <p class="txt">
                예금주 : 강미경
            </p>
        </div>
    </div>
    <div class="cs2">
        <div class="box3"><a href="tel:010-3696-5305">
                <img  src="<?=G5_THEME_IMG_URL?>/f_ico_09.jpg" alt="아이콘" style="width: 20px; height: 20px;">&nbsp;고객센터 연결
            </a></div>
        <div class="box3 box3-custom"><a href="/board/board.php?bo_table=qa">
                <img  src="<?=G5_THEME_IMG_URL?>/f_ico_10.jpg" alt="아이콘" style="width: 20px; height: 20px;">&nbsp;Q&A 게시판
            </a></div>
    </div>

    <div class="info">
        <br>
        <p><strong>676-1 STUDIO</strong></p>
        <p>주소 : 경상남도 창원시 마산회원구 양덕서5길 15 103-303</p>
        <p>대표 : 강미경 &nbsp;&nbsp; 사업자 등록번호 : 708-02-00995</p>
        <p>통신판매업 신고번호 : 2018-창원의창-0109호</p>
        <p>이메일 : tt0545tt@nate.com</p>
    </div>
    <br>
    <p class="copy"style="font-size: 13px;">
        COPYRIGHT © 676-1 STUDIO  All Rights Reserved
    </p>
</div>
<!--div class="cont_menu">
    <div class="row2">
        <ul>
            <li><a href="/board/board.php?bo_table=notice">NOTICE</a></li>
            <li><a href="/board/board.php?bo_table=qa">Q&A</a></li>
            <li><a href="/board/board.php?bo_table=review">REVIEW</a></li>
            <li><a href="https://www.cjlogistics.com/ko/tool/parcel/tracking" target="_blank">DELIVERY</a></li>
        </ul>
    </div>
    <div class="row">
        <ul>
            <li><a href="<?= G5_SHOP_URL ?>/listtype.php?type=4">BEST ITEM</a></li>
            <li><a href="<?= G5_SHOP_URL ?>/listtype.php?type=3">NEW</a></li>
            <li><a href="https://www.instagram.com/mikyeong_kang/" target="_blank">INSTAGRAM</a></li>
            <li><a href="https://blog.naver.com/kyk0545" target="_blank">BLOG</a></li>
        </ul>
    </div>
</div>
<div id="ft">
    <div class="tail_con tail_con1">
        <div class="bold_e">
            CS CENTER
        </div>
        <p class="tail_p1">055-299-6761</p>
        <p class="tail_p1">평일10시 - 6시 토,일,공휴일 휴무</p>
        <p class="tail_p1">tt0545tt@nate.com</p>
    </div>

    <div class="tail_con tail_con2">
        <div class="bold_e">
            BANK ACCOUNT
        </div>
        <p class="tail_p1">경남은행 01036965305</p>
        <p class="tail_p1">예금주 : 강미경</p>
    </div>

    <div class="tail_con tail_con3">
        <div class="bold_e">
            INFORMATION
        </div>
        <p class="tail_p1">상호 : 676-1 STUDIO <span style="width:10px; display: inline-block;"></span> 대표 : 강미경</p>
        <p class="tail_p1">주소 : 창원시 성산구 단정로76번길 30 1층</p>
        <p class="tail_p1">사업자 등록번호 : 708-02-00995</p>
        <p class="tail_p1">통신판매업 신고번호 : 2018-창원의창-0109호</p>
    </div>

    <div class="tail_icon_con">
        <a href="https://www.instagram.com/mikyeong_kang/" target="_blank"><i class="fab fa-instagram"></i></a>
        <a href="https://blog.naver.com/kyk0545" target="_blank"><span class="blog">B</span></a>
    </div>

    <div class="tail_copy">
        <p class="tail_p1">COPYRIGHT ⓒ 676-1 STUDIO ALL Rights Reserved</p>
    </div>
</div-->

<?php
$sec = get_microtime() - $begin_time;
$file = $_SERVER['SCRIPT_NAME'];

if ($config['cf_analytics']) {
    echo $config['cf_analytics'];
}
?>

<script src="<?php echo G5_JS_URL; ?>/sns.js"></script>
<!-- } 하단 끝 -->

<?php
include_once(G5_THEME_PATH . '/tail.sub.php');
?>
