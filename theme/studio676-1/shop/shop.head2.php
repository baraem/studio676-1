<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

if(G5_IS_MOBILE) {
    include_once(G5_THEME_MSHOP_PATH.'/shop.head.php');
    return;
}

include_once(G5_THEME_PATH.'/head.sub.php');
include_once(G5_LIB_PATH.'/outlogin.lib.php');
include_once(G5_LIB_PATH.'/poll.lib.php');
include_once(G5_LIB_PATH.'/visit.lib.php');
include_once(G5_LIB_PATH.'/connect.lib.php');
include_once(G5_LIB_PATH.'/popular.lib.php');
include_once(G5_LIB_PATH.'/latest.lib.php');
?>

<!-- 상단 시작 { -->
<div id="hd">
    <h1 id="hd_h1"><?php echo $g5['title'] ?></h1>

    <!--<div id="skip_to_container"><a href="#container">본문 바로가기</a></div>-->

    <?php if(defined('_INDEX_')) { // index에서만 실행
        include G5_BBS_PATH.'/newwin.inc.php'; // 팝업레이어
    } ?>

    <div class="slide_menu">

        <div class="non_pade">
            <div class="slide_menu_close" style="cursor: pointer">X</div>

            <?php if ($is_member) {?>
                <div class="slide-menu__info">
                    <h3>고객님은‘로그인’상태입니다</h3>
                    <p>회원으로 더 많은 정보와 혜택을 받아보시기 바랍니다</p>
                </div>
                <div class="slide-anchor">
                    <a href="<?php echo G5_BBS_URL;?>/logout.php">로그아웃</a>
                    <a href="/shop/mypage.php" class="slide_menu__anchor1">마이페이지</a>
                </div>
                <!--                    <a href="--><?php //echo G5_BBS_URL;?><!--/logout.php" class="mem_btn">로그아웃</a>-->
            <?}else{?>
                <div class="slide-menu__info">
                    <h3>고객님은‘로그아웃’상태입니다</h3>
                    <p>로그인을 하시면 더 많은 정보와 혜택을 받으실 수 있습니다</p>
                </div>
                <div class="slide-anchor">
                    <a href="<?php echo G5_BBS_URL;?>/login.php">로그인</a>
                    <a href="/board/register.php" class="slide_menu__anchor1">회원가입</a>
                </div>
            <?}?>



            <!--메뉴-->
            <div class="slide_menu_btnCon">
                <!--ABOUT-->
                <!--div class="slide_menu_btn">
                        <a href="<?=G5_BBS_URL?>/sub.php?sub=sub_01&menu=01" >ABOUT</a>
                    </div>

                    <!--ADULT-->
                <!--div class="slide_menu_btn">
                        <a href="<?=G5_SHOP_URL?>/list.php?ca_id=10" >ADULT</a>
                    </div>

                    <!--LIVING-->
                <!--div class="slide_menu_btn">
                        <a href="<?=G5_SHOP_URL?>/list.php?ca_id=20" >LIVING</a>
                    </div>

                    <!--PROMOTION-->
                <!--div class="slide_menu_btn">
                        <a href="<?=G5_BBS_URL?>/board.php?bo_table=promotion" >PROMOTION</a>
                    </div>

                    <!--SHOWROOM-->
                <!--div class="slide_menu_btn">
                        <a href="<?=G5_BBS_URL?>/board.php?bo_table=notice" >SHOWROOM</a>
                    </div-->

                <!--ul class="new_menu">
                        <li class="tlt">shop</li>
                        <li><a href="<?=G5_SHOP_URL?>/listtype.php?type=4">BEST ITEM</a></li>
                        <li><a href="<?=G5_SHOP_URL?>/listtype.php?type=3">NEW</a></li>
                        <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=10">WOMEN</a></li>
                        <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=20">ACC</a></li>
                        <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=30">LIVING</a></li>
                        <li><a href="<?=G5_SHOP_URL?>/listtype.php?type=5">SALE</a></li>
                        <li class="m"><a href="<?=G5_SHOP_URL?>/list.php?ca_id=40">개인결제창</a></li>
                        <li class="tlt">community</li>
                        <li><a href="<?=G5_BBS_URL?>/board.php?bo_table=notice">NOTICE</a></li>
                        <li><a href="<?=G5_BBS_URL?>/board.php?bo_table=qa">Q&A</a></li>
                        <li><a href="<?=G5_BBS_URL?>/board.php?bo_table=review">REVIEW</a></li>
                        <li class="m"><a href="http://www.ilogen.com/d2d/delivery/invoice_search.jsp" target="_blank">DELIVERY</a></li>
                        <li class="tlt">sns</li>
                        <li><a href="https://www.instagram.com/mikyeong_kang/" target="_blank">INSTAGRAM</a></li>
                        <li class="m"><a href="https://blog.naver.com/kyk0545" target="_blank">BLOG</a></li>
                        <li class="tlt">my</li>
                        <li>
                            <--?
                            if( $member['mb_id'] )
                            {?>
                                <a href="<--?=G5_BBS_URL?>/logout.php">LOGOUT</a>
                            <--?}
                            else
                            {?>
                                <a href="<--?=G5_BBS_URL?>/login.php">LOGIN</a>
                            <--?}
                            ?>
                        </li>
                        <li>
                            <--?php
                            if( $member['mb_id'] )
                            {?>
                                <a href="<--?=G5_SHOP_URL?>/mypage.php">MYPAGE</a>
                            <--?}
                            else
                            {?>
                                <a href="<--?=G5_BBS_URL?>/register.php">JOIN US</a>
                            <--?}
                            ?>
                        </li>
                        <li>
                            <a href="<--?=G5_SHOP_URL?>/cart.php">CART</a>
                        </li>
                        <li>LIVING
                            <a href="<--?=G5_SHOP_URL?>/orderinquiry.php">ORDER</a>
                        </li>
                    </ul-->
                <ul id="nav01">
                    <li><a href="#">CLOTH SHOP<i></i></a>
                        <ul>
                            <!--                                <li><a href="--><?//=G5_SHOP_URL?><!--/list.php?ca_id=10">View all</a></li>-->
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=1050">Outer</a></li>
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=1010">Top</a></li>
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=1030">OPS</a></li>
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=1020">Pants/Skirt</a></li>
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=1040">Big Size</a></li>
                        </ul>
                    </li>
                    <li><a href="#">ACC & ETC SHOP<i></i></a>
                        <ul>
                            <!--                                <li><a href="--><?//=G5_SHOP_URL?><!--/list.php?ca_id=a0">VIEW ALL</a></li>-->
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=a010">Shoes</a></li>
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=a020">Bag</a></li>
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=a030">Acc</a></li>
                        </ul>
                    </li>
                    <li><a href="#">LIVING SHOP<i></i></a>
                        <ul>
                            <!--                                <li><a href="--><?//=G5_SHOP_URL?><!--/list.php?ca_id=b0">VIEW ALL</a></li>-->
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=80">Brands</a></li>
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=b020">Kitchen</a></li>
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=b030">Homedeco</a></li>
                            <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=b040">Bathroom</a></li>
                        </ul>
                    </li>
                    <li><a href="<?=G5_SHOP_URL?>/listtype.php?type=5">SALE<i></i></a>
                    </li>
                    <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=40">ONLY YOU<i></i></a>
                    </li>
                </ul>
            </div>

            <?
            if( $member['mb_level'] >= 9 )
            {?>
                <div class="slide_admin_con" style="width: 100%;height: 40px;padding: 0 10px;">
                    <div class="slide_admin" onclick="location.href='<?=G5_ADMIN_URL?>'" style="cursor: pointer;width: 100%;height: 40px;border: 1px solid #444444;text-align: center; font-size: 23px;font-family: Karla;font-weight: bold; line-height: 40px;">
                        ADMIN
                    </div>
                </div>
            <?}
            ?>


            <!--검색창-->
            <div class="slide_menu_find">
                <form name="frmsearch1" action="<?php echo G5_SHOP_URL; ?>/search.php" onsubmit="return search_submit(this);">
                    <label for="sch_str" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
                    <input type="text" name="q" value="<?php echo stripslashes(get_text(get_search_string($q))); ?>" id="sch_str" required>
                    <button type="submit" id="sch_submit"><img src="<?=G5_THEME_URL?>/img/icon/icon_find.png" alt="검색 아이콘" style="width: 16px;height: 16px; display: block;"> </button>

                </form>
                <script>
                    function search_submit(f) {
                        if (f.q.value.length < 2) {
                            alert("검색어는 두글자 이상 입력하십시오.");
                            f.q.select();
                            f.q.focus();
                            return false;
                        }
                        return true;
                    }
                </script>
            </div>
            <div class="tail_icon_con">
                <a href="https://www.instagram.com/mikyeong_kang/" target="_blank"><i class="fab fa-instagram"></i></a>
                <a href="https://blog.naver.com/kyk0545" target="_blank"><span class="blog">B</span></a>
            </div>

        </div>
        <div class="pade" style="cursor: pointer">
        </div>



    </div>


    <script>

        $(function(){
            $('.head_btn').on('click',function(){
                $('.slide_menu').animate({
                    width: "toggle"
                },500)
            });

            $('.slide_menu_close').on('click',function(){
                $('.slide_menu').animate({
                    width: "toggle"
                })
            });
            $('.pade').on('click',function(){
                $('.slide_menu').animate({
                    width: "toggle"
                })
            });
        });
        $(document).ready(function () {



            $('#nav01 > li > a').click(function(){
                if ($(this).attr('class') != 'active'){
                    $('#nav01 li ul').slideUp();
                    $(this).next().slideToggle();
                    $('#nav01 li a').removeClass('active');
                    $(this).addClass('active');
                }
            });
        });

    </script>
    <!--<div id="side_menu">
                <button type="button" id="btn_sidemenu" class="btn_sidemenu_cl"><i class="fa fa-outdent" aria-hidden="true"></i><span class="sound_only">사이드메뉴버튼</span></button>
                <div class="side_menu_wr">
                    <?php /*echo outlogin('theme/shop_basic'); // 아웃로그인 */?>
                    <div class="side_menu_shop">
                        <button type="button" class="btn_side_shop">오늘본상품<span class="count"><?php /*echo get_view_today_items_count(); */?></span></button>
                        <?php /*include(G5_SHOP_SKIN_PATH.'/boxtodayview.skin.php'); // 오늘 본 상품 */?>
                        <button type="button" class="btn_side_shop">장바구니<span class="count"><?php /*echo get_boxcart_datas_count(); */?></span></button>
                        <?php /*include_once(G5_SHOP_SKIN_PATH.'/boxcart.skin.php'); // 장바구니 */?>
                        <button type="button" class="btn_side_shop">위시리스트<span class="count"><?php /*echo get_wishlist_datas_count(); */?></span></button>
                        <?php /*include_once(G5_SHOP_SKIN_PATH.'/boxwish.skin.php'); // 위시리스트 */?>
                    </div>
                    <?php /*include_once(G5_SHOP_SKIN_PATH.'/boxcommunity.skin.php'); // 커뮤니티 */?>

                </div>
            </div>

            <script>
                $(function (){

                    $(".head_btn").on("click", function() {
                        $(".side_menu_wr").toggle();
                        $(".fa-outdent").toggleClass("fa-indent")
                    });

                    $(".btn_side_shop").on("click", function() {
                        $(this).next(".op_area").slideToggle(300).siblings(".op_area").slideUp();
                    });
                });
            </script>-->

    <!--헤더 메뉴-->
    <div class="head_menu">

        <div class="top_menu">
            <ul>
                <?php if ($is_member) {  ?>
                    <li><a href="<?php echo G5_BBS_URL ?>/member_confirm.php?url=<?php echo G5_BBS_URL ?>/register_form.php">MODIFY</a></li>
                    <li><a href="<?php echo G5_BBS_URL ?>/logout.php">LOGOUT</a></li>
                <?php } else {  ?>
                    <li><a href="<?php echo G5_BBS_URL ?>/login.php">LOGIN</a></li>
                    <li><a href="<?php echo G5_BBS_URL ?>/register.php">JOIN</a></li>
                <?php }  ?>
                <li><a href="<?=G5_SHOP_URL?>/cart.php">CART</a></li>
                <li><a href="<?=G5_SHOP_URL?>/orderinquiry.php">ORDER</a></li>
                <li><a href="<?=G5_SHOP_URL?>/mypage.php">MYPAGE</a></li>
            </ul>
        </div>
        <!--토글 메뉴-->
        <div class="head_btn" style="cursor: pointer">
            <div class="head_btns"></div>
        </div>

        <!--로고-->
        <div class="shop_logo">
            <img src="<?=G5_THEME_URL?>/img/bri/shop_logo.png" onclick="location.href='<?=G5_URL?>'" alt="샵로고" style="cursor: pointer">
        </div>



        <div class="head_icon"  style="cursor: pointer">
            <a href="<?=G5_SHOP_URL?>/cart.php">
                <img src="<?=G5_THEME_URL?>/img/bri/cart.png" alt="샵" style="width:22px">
            </a>
        </div>
    </div>
    <!--<div id="hd_wrapper">
        <div id="logo"><a href="<?php /*echo G5_SHOP_URL; */?>/"><img src="<?php /*echo G5_DATA_URL; */?>/common/logo_img" alt="<?php /*echo $config['cf_title']; */?>"></a></div>

    </div>-->
</div>

<?
if( $_SERVER['SCRIPT_URL'] == '/shop/search.php' || $_SERVER['SCRIPT_URL'] == '/shop/cart.php' || $_SERVER['SCRIPT_URL'] == '/shop/orderform.php' || $_SERVER['SCRIPT_URL'] == '/shop/orderinquiryview.php')
{?>
<div id="wrapper_search">
    <?}
    else
    {?>

    <?php
    $sql = "SELECT * FROM main_image_view ORDER BY `order` ASC LIMIT 6";
    $result = sql_query($sql);
    $img_list = array();
    while($row=sql_fetch_array($result))
    {
        $img_list[] = $row;
    }


    ?>


    <div id="headView">


        <div class="icons">
            <div>
                <a href="https://www.instagram.com/mikyeong_kang/">
                    <img src="<?=G5_THEME_IMG_URL?>/icon/insta_icon.png" alt="인스타그램링크">
                </a>
            </div>
            <div>
                <a href="http://676-1studio.co.kr" target="_blank">
                    <img src="<?=G5_THEME_IMG_URL?>/icon/blog_icon.png"  alt="블로그링크">
                </a>
            </div>
        </div>

        <div id="main-slider">
            <?php for($i=0; $i<count($img_list); $i++) { ?>
                <div class="main-slider"> <!--width 100%, height 320px-->
                    <a href="http://<?=$img_list[$i]['link']?>">
                        <img src="/admin/<?= $img_list[$i]['path'] ?>/<?=$img_list[$i]['hashName']?>" alt="메인샘플이미지">
                    </a>
                </div>
            <?php } ?>
        </div>


        <article>

            <section>
                <ul class="sct sct_10 ca10">
                    <?php
                    // 메인에 띄울 상품 조회
                    $sql = "SELECT T1.* FROM BRI_SHOP_item AS T1 INNER JOIN main_item_view AS T2 ON T1.it_id = T2.it_id";
                    $result = sql_query($sql);
                    $item_exposure = array();
                    while($row = sql_fetch_array($result))
                    {
                        $item_exposure[] = $row;
                    }

                    for($i=0; $i<count($item_exposure); $i++) {
                        ?>
                        <li class="bri_list_li" style="float: left;">
                            <div class="bri_list_con" onclick="location.href='http://676-1studio.com/shop/item.php?it_id=<?=$item_exposure[$i]['it_id']?>'" style="cursor: pointer">
                                <div class="bri_list_imgA_con" style="margin-bottom: 13px">
                                    <a class="bri_img_a" href="http://676-1studio.com/shop/item.php?it_id=<?=$item_exposure[$i]['it_id']?>">
                                        <img src="http://676-1studio.com/data/item/<?=$item_exposure[$i]['it_img1']?>" alt="item" title="">
                                    </a>
                                </div>
                                <div class="bri_list_subject_con exposure__custom">
                                    <h3 class="bri_list_subject"><?=$item_exposure[$i]['it_name']?></h3>
                                </div>
                                <div class="bri_list_comment_con">
                                    <h3 class="bri_list_comment"><?=$item_exposure[$i]['it_basic']?></h3>
                                </div>
                                <div class="bri_list_price_wrap exposure__custom">
                                    <div class="bri_list_price_con">
                                        <?php if($item_exposure[$i]['it_cust_price'] != 0) { ?>
                                            <p class="bri_list_cust_price"><?=number_format($item_exposure[$i]['it_cust_price'])?>won</p>
                                        <?php } ?>
                                        <p class="bri_list_price"><?=number_format($item_exposure[$i]['it_price'])?>won</p>
                                    </div>
                                    <?
                                    echo "<div class='item_icon_con'>";
                                    if( $item_exposure[$i]['it_type2'] == 1 ){
                                        ?>
                                        <div class="item_icon">
                                            <img src="<?=G5_THEME_URL?>/img/icon/icon_rec.gif" alt="빅 사이즈"/>
                                        </div>
                                        <?
                                    }
                                    if( $item_exposure[$i]['it_type3'] == 1 )
                                    {?>
                                        <div class="item_icon">
                                            <img src="<?=G5_THEME_URL?>/img/icon/icon_new.png" alt="신상품"/>
                                        </div>
                                    <?}
                                    if( $item_exposure[$i]['it_type4'] == 1 )
                                    {?>
                                        <div class="item_icon">
                                            <img src="<?=G5_THEME_URL?>/img/icon/icon_best.png" alt="베스트" style="width: 34px"/>
                                        </div>
                                    <?}
                                    if( $item_exposure[$i]['it_type5'] == 1 )
                                    {?>
                                        <div class="item_icon">
                                            <img src="<?=G5_THEME_URL?>/img/icon/icon_discount.jpg" alt="할인" style="width: 34px"/>
                                        </div>
                                    <?}
                                    if( $item_exposure[$i]['it_soldout'] == 1 )
                                    {?>
                                        <div class="item_icon">
                                            <img src="<?=G5_THEME_URL?>/img/icon/icon_soldout.png" alt="품절상품" style="width:53px;height: 13px"/>
                                        </div>
                                    <?}
                                    echo "</div>";
                                    ?>
                                </div>
                            </div>
                        </li>
                    <?php } ?>

                </ul>
            </section>
            <section>
                <table id="nav-table">
                    <tr>
                        <td><a href="/shop/list.php?ca_id=1050">Outer</a></td>
                        <td><a href="/shop/list.php?ca_id=1010">Top</a></td>
                        <td><a href="/shop/list.php?ca_id=1030">Ops</a></td>
                        <td><a href="/shop/list.php?ca_id=1020">Pants/Skirt</a></td>
                    </tr>
                    <tr>
                        <td><a href="/shop/list.php?ca_id=a010">Shoes</a></td>
                        <td><a href="/shop/list.php?ca_id=a020">Bag</a></td>
                        <td><a href="/shop/list.php?ca_id=a030">Acc</a></td>
                        <td><a href="http://676-1studio.co.kr/">676-1 마켓</a></td>
                    </tr>
                    <tr>
                        <td><a href="/shop/list.php?ca_id=80">brand</a></td>
                        <td><a href="/shop/list.php?ca_id=b020">Kitchen</a></td>
                        <td><a href="/shop/list.php?ca_id=b030">Homedeco</a></td>
                        <td><a href="/shop/list.php?ca_id=b040">Bathroom</a></td>
                    </tr>
                </table>
            </section>
        </article>
    </div>


    <script>
        $('#main-slider').slick({
            dots: true,
            infinite: true,
            speed: 500,
            fade: true,
            autoplay: true,
            cssEase: 'linear'
        });
    </script>

    <div id="wrapper" style="padding-top: 130px;">

        <?}
        ?>

        <!-- } 상단 끝 -->



        <!-- 콘텐츠 시작 { -->
        <div id="container">

            <?php /*if ((!$bo_table || $w == 's' ) && !defined('_INDEX_')) { */?><!--<div id="wrapper_title"><?php /*echo $g5['title'] */?></div>--><?php /*} */?>

            <!--제품 항목-->
            <?
            if( $_GET['ca_id'] )
            {

            $ca_id = substr($_GET['ca_id'], 0, 2 );

            $itemSql = "
                    SELECT
                      *
                     FROM
                      BRI_SHOP_category
                    WHERE
                      ca_id LIKE '{$ca_id}%'
                    ORDER BY ca_order
                ";

            $itemResult = sql_query($itemSql);
            while ( $itemRow = $itemResult -> fetch_array() )
            {
                $itemList[] = $itemRow;
            }

            ?>
        <//?include_once (G5_THEME_PATH."/cont_menu.php"); ?>

        <div class="shop_title">
            <a href="<?=G5_SHOP_URL?>/list.php?ca_id=<?=$ca_id?>" ><?=$ca['ca_name']?></a>
        </div>

        <div align="center">
            <?php
            if($ca_id != 40){
                ?>
                <ul class="shop_list_ul10">
                    <?php for($i=0; $i<count($itemList); $i++){ ?>
                        <?php
                        if($i == 0){
                            $itemList[$i]['ca_name'] = 'VIEW ALL';
                        }
                        ?>
                        <li><a href="<?=G5_SHOP_URL?>/list.php?ca_id=<?=$itemList[$i]['ca_id']?>"><?=$itemList[$i]['ca_name']?></a></li>
                    <?}?>
                </ul>
                <?
            }
            ?>
        </div>
    <!--제품 항목2-->
        <div class="shop_class">
            <?
            /*제품카테고리 추가시 자동증가*/

            foreach ( $itemList as $itemKey => $itemVal )
            {
                if( $ca_id == $itemVal['ca_id'] ) continue;

                if( $itemVal === end($itemList) )
                {
                    $item1 = $itemVal['ca_id'];
                    $item2 = $itemVal['ca_name'];
                    continue;
                }
                ?>
                <div class="shop_class_btn shop_class_btn_<?=$itemVal['ca_id']?>" onclick="location.href='<?=G5_SHOP_URL?>/list.php?ca_id=<?=$itemVal['ca_id']?>'" style="cursor: pointer"><?=$itemVal['ca_name']?></div>/
            <?}
            ?>
            <div class="shop_class_btn shop_class_btn_<?=$item1?>" onclick="location.href='<?=G5_SHOP_URL?>/list.php?ca_id=<?=$item1?>'" style="cursor: pointer"><?=$item2?></div>
        </div>
    <?}
    ?>
        <!-- 글자크기 조정 display:none 되어 있음 시작 { -->

        <!-- } 글자크기 조정 display:none 되어 있음 끝 -->