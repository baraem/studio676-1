<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// 인스타 날짜 갱신일 보기
$insta_sql = "SELECT ig_date FROM `BRI_instagram` LIMIT 1";
$insta_row = sql_fetch_array(sql_query($insta_sql));
$insta_date = "X";
if($insta_row['ig_date']){
    $insta_date = $insta_row['ig_date'];
}

$api_url = G5_THEME_URL."/api/instagram";
?>
<tr>
    <th scope="row">인스타그램 피드 갱신</th>
    <td colspan="3">
        <a id="insta_update" href="#" style="text-decoration: underline; color: deepskyblue">업데이트</a>
        <span>마지막 갱신 : <?php echo $insta_date; ?> <img src="<?=$api_url?>/img/ajax-loader.gif" alt="로딩" class="ajax_loading" style="display: none;"></span>
        <script>
            $('#insta_update').click(function (e) {

                e.preventDefault();

                $.ajax ({
                    type : "GET",         // GET 또는 POST
                    url : '<?=$api_url?>/instagram.php',   // 서버측에서 가져올 페이지
                    data: "send=confirm",
                    dataType : 'html',
                    beforeSend: function() {      // ajax 요청하기전에 실행되는 함수
                        $(".ajax_loading").css("display", "inline-block");
                    },
                    // html , javascript, text, xml, jsonp 등이 있다
                    success : function(data) {
                        // 정상적으로 완료되었을 경우에 실행된다
                        if(data == true){
                            alert("성공적으로 업데이트 했습니다.");
                            location.reload();
                        }else {
                            alert("업데이트 실패!\n"+data);

                        }
                    },
                    complete : function () {   // 정상이든 비정상인든 실행이 완료될 경우 실행될 함수
                        $(".ajax_loading").css("display", "none");
                    }
                });
            });
        </script>
    </td>
</tr>


