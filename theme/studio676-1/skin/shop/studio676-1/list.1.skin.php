<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_SHOP_SKIN_URL.'/style.css">', 0);
?>
<!-- 상품진열 10 시작 { -->
<?php
    //상품을 검색한다.
    $p = 0;

    while ( $row = $result -> fetch_array() )
    {
        $list[] = $row;
    }

?>

    <?
    if(!$_GET['ca_id']){
        if($list[0]['ca_id']){
            $ca_id = "ca".$this->ca_id;
        }
    }
        if( $list )
        {?>
            <ul class="sct sct_10 <?=$ca_id?>">
                <?
                //제품을 나열함
                $i = 0;
                foreach ( $list as $k => $v )
                {
                    $i++;
                    //이미지 구함
                    $item = explode('/',$v['it_img1']);

                    if( $item[0] == 0 )
                    {
                        $img_url = '/img/no_img.png';
                    }
                    else
                    {
                        $img_url = G5_DATA_URL.'/item/'.$item[0].'/'.$item[1];
                    } ?>
                        <li class="bri_list_li" style="float: left;">


                    <div class="bri_list_con" onclick="location.href='<?=G5_SHOP_URL?>/item.php?it_id=<?=$v['it_id']?>'" style="cursor: pointer">
                        <div class="bri_list_imgA_con" style="margin-bottom: 13px">
                            <a class="bri_img_a" href="<?=G5_SHOP_URL?>/item.php?it_id=<?=$v['it_id']?>">
                                <img src="<?=$img_url?>" alt="item" />
                            </a>


                        </div>

                        <div class="bri_list_subject_con">
                            <h3 class="bri_list_subject"><?=$v['it_name']?></h3>
                        </div>

                        <div class="bri_list_comment_con">
                            <h3 class="bri_list_comment"><?=$v['it_basic']?></h3>
                        </div>

                        <div class="bri_list_price_wrap">

                            <div class="bri_list_price_con">
                                <?php
                                    if($v['it_cust_price']){
                                        ?>
                                        <p class="bri_list_cust_price "><?=number_format($v['it_cust_price'])?>won</p>
                                    <?php } ?>
                                <p class="bri_list_price"><?=number_format($v['it_price'])?>won</p>
                            </div>

                            <?
                            echo "<div class='item_icon_con'>";
                            if( $v['it_type2'] == 1 ){
                                ?>

                                    <img src="<?=G5_THEME_URL?>/img/icon/icon_rec.gif" alt="빅 사이즈"/>

                                <?
                            }
                            if( $v['it_type3'] == 1 )
                            {?>

                                    <img src="<?=G5_THEME_URL?>/img/icon/icon_new.png" alt="신상품"/>

                            <?}
                            if( $v['it_type4'] == 1 )
                            {?>

                                    <img src="<?=G5_THEME_URL?>/img/icon/icon_best.png" alt="베스트" style="width: 34px"/>

                            <?}
                            if( $v['it_type5'] == 1 )
                            {?>

                                    <img src="<?=G5_THEME_URL?>/img/icon/icon_discount.jpg" alt="할인" style="width: 34px"/>

                            <?}
                            if( $v['it_soldout'] == 1 )
                            {?>
<!--                                <div class="item_icon">-->
                                    <img src="<?=G5_THEME_URL?>/img/icon/icon_soldout.png" alt="품절상품" style="width:53px;height: 13px"/>
<!--                                </div>-->
                            <?}
                            echo "</div>";
                            ?>

                        </div>




                    </div>
                    </li>

                <?}
                ?>
            </ul>
        <?}
        else
        {?>
            <ul class="sct sct_10">
                <li style="text-align: center;margin: 50px 0;">
                    등록된 상품이 없습니다.
                </li>
            </ul>
        <?}
    ?>

    <script>
        //제품 더 불러오기
        function plus_list(p, ca_id){

            console.log(p);

            var data = new Object();
            if(ca_id){
                data[0]  =  ca_id; //제품 항목
                var ca_Class = ".ca"+ca_id;
            }else{
                data[0]  =  '<?=$_GET['ca_id']?>'; //제품 항목
            }

            data[1]  =  p; //제품 페이징
            data[2]  = '<?=$_GET['sort']?>';    //정렬조건1
            data[3]  = '<?=$_GET['sortodr']?>'; //정렬조건2
            console.log(data);
            $.ajax({
                type       : 'post',
                url        : '<?=G5_THEME_URL?>/ajax/plus_listAjax.php',
                data       : data,
                beforeSend : function(){
                    $('.loading').css('display', 'block');
                },
                complete   : function(){
                    $('.loading').css('display', 'none');
                },
                success    : function(response){
                    var data = JSON.parse(response);

                    if( data )
                    {
                        $html = '';

                        var img_url = '<?=G5_DATA_URL?>/item/';
                        var item_url = '<?=G5_SHOP_URL?>/item.php?it_id=';
                        $.each(data, function(key, val){
                            var item = val.it_img1.split('/');

                            $html += '<li class="bri_list_li" style="float: left">';

                            $html +=    '<div class="bri_list_con" onclick="' + 'location.href=' + "'" + item_url + val.it_id + "'" + '" style="cursor: pointer">';
                            $html +=        '<div class="bri_list_imgA_con" style="margin-bottom: 13px">';
                            $html +=            '<a class="bri_img_a" href="' + item_url + val.it_id + '">';
                            $html +=                '<img src="' + img_url + item[0] + '/' + item[1] + '"/>';
                            $html +=            '</a>';

                            $html +=        '</div>';
                            $html +=        '<div class="bri_list_subject_con">';
                            $html +=            '<h3 class="bri_list_subject">' + val.it_name + '</h3>';
                            $html +=        '</div>';

                            $html +=        '<div class="bri_list_comment_con">';
                            $html +=            '<h3 class="bri_list_comment">' + val.it_basic + '</h3>';
                            $html +=        '</div>';

                            $html +=        '<div class="bri_list_price_con">';
                            if(val.it_cust_price != 0) {
                            $html +=            '<p class="bri_list_price bri_list_cust_price ">' + numfom(val.it_cust_price) + 'won' + '</p>';
                            }else {
                                $html +=            '<p class="bri_list_price bri_list_cust_price" style="font-size: 0 !important; height: 13px;">\' + numfom(val.it_cust_price) + \'won\' + \'</p>';
                            }
                            $html +=            '<p class="bri_list_price">' + numfom(val.it_price) + 'won' + '</p>';
                            $html +=        '</div>';

                            $html += '<div class=\'item_icon_con\'>';
                            if(val.it_type2 === "1"){
                                $html +=  '<img src="<?=G5_THEME_URL?>/img/icon/icon_rec.gif" alt="빅 사이즈"/>';
                            }
                            if(val.it_type3 === "1"){
                                $html +=  '<img src="<?=G5_THEME_URL?>/img/icon/icon_new.png" alt="신상품"/>';
                            }
                            if(val.it_type4 === "1"){
                                $html +=  '<img src="<?=G5_THEME_URL?>/img/icon/icon_best.png" alt="베스트" style="width: 34px"/>';
                            }
                            if(val.it_type5 === "1"){
                                $html +=  '<img src="<?=G5_THEME_URL?>/img/icon/icon_discount.jpg" alt="할인" style="width: 34px"/>';
                            }
                            if(val.it_soldout === "1"){
                                $html +=  '<img src="<?=G5_THEME_URL?>/img/icon/icon_soldout.png" alt="품절상품" style="width:53px;height: 13px"/>';
                            }
                            $html += '</div>';

                            $html +=    '</div>';
                            $html += '</li>';
                        });
                        if(ca_Class){
                            $('.sct_10'+ca_Class).append($html);
                        }else{
                            $('.sct_10').append($html);
                        }

                    }
                    else
                    {
                        alert('더이상 상품 없습니다.');
                        return false;
                    }
                }
            })
        }

        function numfom(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

    </script>
<!-- } 상품진열 10 끝 -->