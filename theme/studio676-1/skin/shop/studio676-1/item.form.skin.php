<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_SHOP_CSS_URL.'/style.css">', 0);

?>

<script>
    $(function(){
        var script_url = '<?=$_SERVER['SCRIPT_URL']?>';

        if( script_url == '/shop/item/item.php' || script_url == '/shop/item.php')
        {
            $('#sit_pvi').css('padding','0px');
        }
    })
</script>

<form name="fitem" method="post" action="<?php echo $action_url; ?>" onsubmit="return fitem_submit(this);">
<input type="hidden" name="it_id[]" value="<?php echo $it_id; ?>">
<input type="hidden" name="sw_direct">
<input type="hidden" name="url">

<div id="sit_ov_wrap">
    <!-- 상품이미지 미리보기 시작 { -->
    <div id="sit_pvi">
        <div id="sit_pvi_big">
        <?php
        //기존 소스
/*        $big_img_count = 0;
        $thumbnails = array();
        for($i=1; $i<=10; $i++) {
            if(!$it['it_img'.$i])
                continue;

            $img = get_it_thumbnail($it['it_img'.$i], $default['de_mimg_width'], $default['de_mimg_height']);

            if($img) {
                // 썸네일
                $thumb = get_it_thumbnail($it['it_img'.$i], 60, 60);
                $thumbnails[] = $thumb;
                $big_img_count++;

                echo '<a href="'.G5_SHOP_URL.'/largeimage.php?it_id='.$it['it_id'].'&amp;no='.$i.'" target="_blank" class="popup_item_image">'.$img.'</a>';
            }
        }

        if($big_img_count == 0) {
            echo '<img src="'.G5_SHOP_URL.'/img/no_image.gif" alt="">';
        }
        */?>
        <?php

        if( $it['it_img1'] )
        {
            $img1 = explode('/',$it['it_img1']);
            $img_path = G5_DATA_URL.'/item/'.$img1[0].'/'.$img1[1];
        }
        else
        {
            $img_path = '/img/no_img.png';
        }
        ?>
            <img src="<?=$img_path?>" alt="상세보기" style="width: 100%; height: 100%;max-height: 677px"/>
        <?
        ?>
        </div>

        <!--sns공유기능 필요할수 있음-->
        <!--<div id="sit_star_sns">
            <?php /*if ($star_score) { */?>
            <span class="sound_only">고객평점</span>
            <img src="<?php /*echo G5_SHOP_URL; */?>/img/s_star<?php /*echo $star_score*/?>.png" alt="" class="sit_star" width="100">
            별<?php /*echo $star_score*/?>개
            <?php /*} */?>
            <span class="st_bg"></span> <i class="fa fa-commenting-o" aria-hidden="true"></i><span class="sound_only">리뷰</span> <?php /*echo $it['it_use_cnt']; */?>
            <span class="st_bg"></span> <i class="fa fa-heart-o" aria-hidden="true"></i><span class="sound_only">위시</span> <?php /*echo get_wishlist_count_by_item($it['it_id']); */?>
            <button type="button" class="btn_sns_share"><i class="fa fa-share-alt" aria-hidden="true"></i><span class="sound_only">sns 공유</span></button>
            <div class="sns_area"><?php /*echo $sns_share_links; */?> <a href="javascript:popup_item_recommend('<?php /*echo $it['it_id']; */?>');" id="sit_btn_rec"><i class="fa fa-envelope-o" aria-hidden="true"></i><span class="sound_only">추천하기</span></a></div>
        </div>
        <script>
        $(".btn_sns_share").click(function(){
            $(".sns_area").show();
        });
        $(document).mouseup(function (e){
            var container = $(".sns_area");
            if( container.has(e.target).length === 0)
            container.hide();
        });


        </script>-->
    </div>
    <!-- } 상품이미지 미리보기 끝 -->

    <!-- 상품 요약정보 및 구매 시작 { -->
    <section id="sit_ov" class="2017_renewal_itemform">

        <!--제품 이름-->
        <div class="items item_subject">
            <?=$it['it_name']?>
        </div>

        <?php
        if($it['it_cust_price'] != 0) { ?>

            <div class="items item_price">
                <input type="hidden" name="it_cust_price" id="it_cust_price" value="<?=$it['it_cust_price']?>"/>
                <p class="item_th">정상가</p>
                <p class="item_td td_cust"><?=number_format($it['it_cust_price'])?>&nbsp;won</p>
            </div>

        <?php } ?>



        <!--제품 가격-->
        <div class="items item_price">
            <input type="hidden" name="it_price" id="it_price" value="<?=$it['it_price']?>"/>
            <p class="item_th">가격</p>
            <p class="item_td"><?=number_format($it['it_price'])?>&nbsp;won</p>
        </div>

        <?php
            //제품 배송비 계산. 쇼핑몰 기본 가격을 사용하기 위해서 만듬

        if( !$it['it_sc_price'] ) {
            $texSql = "
                SELECT
                    de_send_cost_limit, de_send_cost_list
                 FROM
                    BRI_SHOP_default
            ";
            $texResult = sql_query($texSql);
            while ($texRow = $texResult->fetch_array()) {
                $texList[] = $texRow;
            }

            $tex = preg_split('/[\;]/', $texList[0]['de_send_cost_limit']);
            $tex_price = preg_split('/[\;]/', $texList[0]['de_send_cost_list']);
            //가격이 첫번째 배송비 보다 높을 경우
            if ($tex[0] < $it['it_price']) {

                //가격이 두번째 배송비보다 높을 경우
                if ($tex[1] < $it['it_price']) {
                    //가격이 3번째 배송비와 같거나 높을경우
                    if ($tex[2] <= $it['it_price']) {
                        $tex_val = $tex_price[2];
                    }
                } else {
                    $tex_val = $tex_price[1];
                }
            } else {//가격이 기본 배송비의 첫번째 값보다 작을경우
                $tex_val = $tex_price[0];
            }
        }
        else
        {
            $tex_val = $it['it_sc_price'];
        }
        ?>
        
        <!--제품 배송비-->
        <?php
        if($it['ca_id'] != 40) {  // 개인결제창(40) 일때 배송비 안뜨게 함
            ?>
            <div class="items item_tex">
                <p class="item_th abc">배송비</p>
                <p class="item_td" align="right">
                    <?=number_format($tex_val)?>&nbsp;won<br/>

                    <?php
                    // 카테고리 7000번대(furniture)에서 '~만원이상구매시무료' 안보이게 함
                    if(7000 < (int)$it['ca_id'] && (int)$it['ca_id'] < 8000) {?>
                    <? }
                    else {?>
                        <span style="color: #000000">(<?= number_format($default['de_send_cost_limit']) ?>원 이상 구매 시 무료)</span>
                    <?}
                    ?>
                </p>
                <input type="hidden" name="tex_price_con" value="<?=$tex_val?>"/>
            </div>
            <?
        }
        ?>

<!--        <div class="items item_tex">-->
<!--            <p class="item_th">배송비</p>-->
<!--            <p class="item_td" align="right">-->
<!--                --><?//=number_format($tex_val)?><!--&nbsp;won<br/>-->
<!--                <span style="color: #000000">(--><?//=number_format($default['de_send_cost_limit'])?><!--원 이상 구매 시 무료)</span>-->
<!--            </p>-->
<!--            <input type="hidden" name="tex_price_con" value="--><?//=$tex_val?><!--"/>-->
<!--        </div>-->

        <?php if($is_orderable) { ?>
        <p id="sit_opt_info">
            상품 선택옵션 <?php echo $option_count; ?> 개, 추가옵션 <?php echo $supply_count; ?> 개
        </p>
        <?php } ?>
        <div class="sit_info" style="display: none">
            <table class="sit_ov_tbl">
            <colgroup>
                <col class="grid_3">
                <col>
            </colgroup>
            <tbody>

            <?php
            /* 재고 표시하는 경우 주석 해제
            <tr>
                <th scope="row">재고수량</th>
                <td><?php echo number_format(get_it_stock_qty($it_id)); ?> 개</td>
            </tr>
            */
            ?>

            <tr>
                <th><?php echo $ct_send_cost_label; ?></th>
                <td><?php echo $sc_method; ?></td>
            </tr>
            <?php if($it['it_buy_min_qty']) { ?>
            <tr>
                <th>최소구매수량</th>
                <td><?php echo number_format($it['it_buy_min_qty']); ?> 개</td>
            </tr>
            <?php } ?>
            <?php if($it['it_buy_max_qty']) { ?>
            <tr>
                <th>최대구매수량</th>
                <td><?php echo number_format($it['it_buy_max_qty']); ?> 개</td>
            </tr>
            <?php } ?>
            </tbody>
            </table>
        </div>
        <?php
        if($option_item) {
        ?>
        <!-- 선택옵션 시작 { -->
        <section class="sit_option">
 
            <?php // 선택옵션
            echo $option_item;
            ?>
        </section>
        <!-- } 선택옵션 끝 -->
        <?php
        }
        ?>

        <?php
        if($supply_item) {
        ?>
        <!-- 추가옵션 시작 { -->
        <section  class="sit_option">
            <h3>추가옵션</h3>
            <?php // 추가옵션
            echo $supply_item;
            ?>
        </section>
        <!-- } 추가옵션 끝 -->
        <?php
        }
        ?>

        <?php if ($is_orderable) { ?>
        <!-- 선택된 옵션 시작 { -->
        <section id="sit_sel_option">
            <h3>선택된 옵션</h3>
            <?php
            if(!$option_item) {
                if(!$it['it_buy_min_qty'])
                    $it['it_buy_min_qty'] = 1;
            ?>
            <ul id="sit_opt_added">
                <li class="sit_opt_list">
                    <input type="hidden" name="io_type[<?php echo $it_id; ?>][]" value="0">
                    <input type="hidden" name="io_id[<?php echo $it_id; ?>][]" value="">
                    <input type="hidden" name="io_value[<?php echo $it_id; ?>][]" value="<?php echo $it['it_name']; ?>">
                    <input type="hidden" class="io_price" value="0">
                    <input type="hidden" class="io_stock" value="<?php echo $it['it_stock_qty']; ?>">
                    <div class="opt_name">
                        <span class="sit_opt_subj"><?php echo $it['it_name']; ?></span>
                    </div>
                    <div class="opt_count tt">
                        <span class="sit_opt_prc" style="margin-right: 10px">+<?=number_format($it['it_price'])?>won</span>
                        <label for="ct_qty_<?php echo $i; ?>" class="sound_only">수량</label>
                        <button type="button" class="sit_qty_minus"><i class="fa fa-minus" aria-hidden="true"></i><span class="sound_only">감소</span></button>
                        <input type="text" name="ct_qty[<?php echo $it_id; ?>][]" value="<?php echo $it['it_buy_min_qty']; ?>" id="ct_qty_<?php echo $i; ?>" class="num_input" size="5">
                        <button type="button" class="sit_qty_plus"><i class="fa fa-plus" aria-hidden="true"></i><span class="sound_only">증가</span></button>
                    </div>
                </li>
            </ul>
            <script>
                $(function() {
                    price_calculate();
                });

            </script>
            <?php } ?>
        </section>
        <!-- } 선택된 옵션 끝 -->

        <!-- 총 구매액 -->
        <div id="sit_tot_price"></div>

        <?php } ?>

        <?php if($is_soldout) { ?>
        <p id="sit_ov_soldout">상품의 재고가 부족하여 구매할 수 없습니다.</p>
        <?php } ?>


        <div id="sit_ov_btn">
            <?php if ($is_orderable) { ?>
            <button type="submit" onclick="document.pressed=this.value;" value="바로구매" id="sit_btn_buy">BUY IT NOW</button>
            <?php if($it['it_id'] != "1583040845") { ?><button type="submit" onclick="document.pressed=this.value;" value="장바구니" id="sit_btn_cart">CART</button><?php } ?>
            <?php } ?>
            <?php if(!$is_orderable && $it['it_soldout'] && $it['it_stock_sms']) { ?>
            <a href="javascript:popup_stocksms('<?php echo $it['it_id']; ?>');" id="sit_btn_alm"><i class="fa fa-bell-o" aria-hidden="true"></i> 재입고알림</a>
            <?php } ?>
            <!--<//?php if($it['it_id'] != "1583040845") {?><a href="javascript:item_wish(document.fitem, '<//?php echo $it['it_id']; ?>');" id="sit_btn_wish">WISH</a><//?php } ?>-->
            <?php if ($naverpay_button_js) { ?>
            <div class="itemform-naverpay"><?php echo $naverpay_request_js.$naverpay_button_js; ?></div>
            <?php } ?>
        </div>

        <script>
        // 상품보관
        function item_wish(f, it_id)
        {
            f.url.value = "<?php echo G5_SHOP_URL; ?>/wishupdate.php?it_id="+it_id;
            f.action = "<?php echo G5_SHOP_URL; ?>/wishupdate.php";
            f.submit();
        }

        // 추천메일
        function popup_item_recommend(it_id)
        {
            if (!g5_is_member)
            {
                if (confirm("회원만 추천하실 수 있습니다."))
                    document.location.href = "<?php echo G5_BBS_URL; ?>/login.php?url=<?php echo urlencode(G5_SHOP_URL."/item.php?it_id=$it_id"); ?>";
            }
            else
            {
                url = "./itemrecommend.php?it_id=" + it_id;
                opt = "scrollbars=yes,width=616,height=420,top=10,left=10";
                popup_window(url, "itemrecommend", opt);
            }
        }

        // 재입고SMS 알림
        function popup_stocksms(it_id)
        {
            url = "<?php echo G5_SHOP_URL; ?>/itemstocksms.php?it_id=" + it_id;
            opt = "scrollbars=yes,width=616,height=420,top=10,left=10";
            popup_window(url, "itemstocksms", opt);
        }
        </script>
    </section>
    <!-- } 상품 요약정보 및 구매 끝 -->

</div>

</form>


<script>
$(function(){
    // 상품이미지 첫번째 링크
    $("#sit_pvi_big a:first").addClass("visible");

    // 상품이미지 미리보기 (썸네일에 마우스 오버시)
    $("#sit_pvi .img_thumb").bind("mouseover focus", function(){
        var idx = $("#sit_pvi .img_thumb").index($(this));
        $("#sit_pvi_big a.visible").removeClass("visible");
        $("#sit_pvi_big a:eq("+idx+")").addClass("visible");
    });

    // 상품이미지 크게보기
    $(".popup_item_image").click(function() {
        var url = $(this).attr("href");
        var top = 10;
        var left = 10;
        var opt = 'scrollbars=yes,top='+top+',left='+left;
        popup_window(url, "largeimage", opt);

        return false;
    });
});

function fsubmit_check(f)
{
    // 판매가격이 0 보다 작다면
    if (document.getElementById("it_price").value < 0) {
        alert("전화로 문의해 주시면 감사하겠습니다.");
        return false;
    }

    if($(".sit_opt_list").size() < 1) {
        alert("상품의 선택옵션을 선택해 주십시오.");
        return false;
    }

    var val, io_type, result = true;
    var sum_qty = 0;
    var min_qty = parseInt(<?php echo $it['it_buy_min_qty']; ?>);
    var max_qty = parseInt(<?php echo $it['it_buy_max_qty']; ?>);
    var $el_type = $("input[name^=io_type]");

    $("input[name^=ct_qty]").each(function(index) {
        val = $(this).val();

        if(val.length < 1) {
            alert("수량을 입력해 주십시오.");
            result = false;
            return false;
        }

        if(val.replace(/[0-9]/g, "").length > 0) {
            alert("수량은 숫자로 입력해 주십시오.");
            result = false;
            return false;
        }

        if(parseInt(val.replace(/[^0-9]/g, "")) < 1) {
            alert("수량은 1이상 입력해 주십시오.");
            result = false;
            return false;
        }

        io_type = $el_type.eq(index).val();
        if(io_type == "0")
            sum_qty += parseInt(val);
    });

    if(!result) {
        return false;
    }

    if(min_qty > 0 && sum_qty < min_qty) {
        alert("선택옵션 개수 총합 "+number_format(String(min_qty))+"개 이상 주문해 주십시오.");
        return false;
    }

    if(max_qty > 0 && sum_qty > max_qty) {
        alert("선택옵션 개수 총합 "+number_format(String(max_qty))+"개 이하로 주문해 주십시오.");
        return false;
    }

    return true;
}

// 바로구매, 장바구니 폼 전송
function fitem_submit(f)
{
    f.action = "<?php echo $action_url; ?>";
    f.target = "";

    if (document.pressed == "장바구니") {
        f.sw_direct.value = 0;
    } else { // 바로구매
        f.sw_direct.value = 1;
    }

    // 판매가격이 0 보다 작다면
    if (document.getElementById("it_price").value < 0) {
        alert("전화로 문의해 주시면 감사하겠습니다.");
        return false;
    }

    if($(".sit_opt_list").size() < 1) {
        alert("상품의 선택옵션을 선택해 주십시오.");
        return false;
    }

    var val, io_type, result = true;
    var sum_qty = 0;
    var min_qty = parseInt(<?php echo $it['it_buy_min_qty']; ?>);
    var max_qty = parseInt(<?php echo $it['it_buy_max_qty']; ?>);
    var $el_type = $("input[name^=io_type]");

    $("input[name^=ct_qty]").each(function(index) {
        val = $(this).val();

        if(val.length < 1) {
            alert("수량을 입력해 주십시오.");
            result = false;
            return false;
        }

        if(val.replace(/[0-9]/g, "").length > 0) {
            alert("수량은 숫자로 입력해 주십시오.");
            result = false;
            return false;
        }

        if(parseInt(val.replace(/[^0-9]/g, "")) < 1) {
            alert("수량은 1이상 입력해 주십시오.");
            result = false;
            return false;
        }

        io_type = $el_type.eq(index).val();
        if(io_type == "0")
            sum_qty += parseInt(val);
    });

    if(!result) {
        return false;
    }

    if(min_qty > 0 && sum_qty < min_qty) {
        alert("선택옵션 개수 총합 "+number_format(String(min_qty))+"개 이상 주문해 주십시오.");
        return false;
    }

    if(max_qty > 0 && sum_qty > max_qty) {
        alert("선택옵션 개수 총합 "+number_format(String(max_qty))+"개 이하로 주문해 주십시오.");
        return false;
    }

    return true;
}
</script>
<?php /* 2017 리뉴얼한 테마 적용 스크립트입니다. 기존 스크립트를 오버라이드 합니다. */ ?>
<script src="<?php echo G5_JS_URL; ?>/shop.override.js"></script>