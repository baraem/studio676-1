<?php
// 설명 : 이 php는 제품 view의 상세정보를 기술하는 php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_SHOP_CSS_URL.'/style.css">', 0);
?>

<script src="<?php echo G5_JS_URL; ?>/viewimageresize.js"></script>

<!--상품정보 한 공간에서 시작-->
<div class="item_datas">
    
    <!--제품 상세정보 메뉴-->
    <div class="item_datas_menu_bar">
        <div class="item_datas_menu_btn" id="item_datas_menu_btn1">
            DETAIL
        </div>
        <div class="item_datas_menu_btn" id="item_datas_menu_btn2">
            INFO
        </div>
        <div class="item_datas_menu_btn" id="item_datas_menu_btn3">
            REVIEW
        </div>
        <div class="item_datas_menu_btn" id="item_datas_menu_btn4">
            Q&A
        </div>
    </div>

    <!--제품의 상세정보 view-->
    <div class="item_datas_view_con">

        <!--보여줄 페이지 DETAIL-->
        <div class="item_datas_view_contents item_datas_view_detail">




            <?php if ($it['it_explan']) { // 상품 상세설명 ?>
                <!--<h3>상품 상세설명ㅇ</h3>-->
                <div id="sit_inf_explan">
                    <?php echo conv_content($it['it_explan'], 1); ?>
                </div>
            <?php } ?>

            <?php
/*            if ($it['it_info_value']) { // 상품 정보 고시
                $info_data = unserialize(stripslashes($it['it_info_value']));
                if(is_array($info_data)) {
                    $gubun = $it['it_info_gubun'];
                    $info_array = $item_info[$gubun]['article'];
                    */?><!--
                    <h3>상품 정보 고시</h3>
                    <table id="sit_inf_open">
                        <colgroup>
                            <col class="grid_4">
                            <col>
                        </colgroup>
                        <tbody>
                        <?php
/*                        foreach($info_data as $key=>$val) {
                            $ii_title = $info_array[$key][0];
                            $ii_value = $val;
                            */?>
                            <tr>
                                <th scope="row"><?php /*echo $ii_title; */?></th>
                                <td><?php /*echo $ii_value; */?></td>
                            </tr>
                        <?php /*} //foreach*/?>
                        </tbody>
                    </table>
                    <!-- 상품정보고시 end -->
                    <?php
/*                } else {
                    if($is_admin) {
                        echo '<p>상품 정보 고시 정보가 올바르게 저장되지 않았습니다.<br>config.php 파일의 G5_ESCAPE_FUNCTION 설정을 addslashes 로<br>변경하신 후 관리자 &gt; 상품정보 수정에서 상품 정보를 다시 저장해주세요. </p>';
                    }
                }
            }
            */?>
        </div>

        <!--보여줄 페이지 INFO-->
        <div class="item_datas_view_contents item_datas_view_info">
            <?php if ($default['de_baesong_content']) { // 배송정보 내용이 있다면 ?>
                <!-- 배송정보 시작 { -->
                <section id="sit_dvr">
                    <!--<h2>배송정보</h2>-->

                    <?php echo conv_content($default['de_baesong_content'], 1); ?>
                </section>
                <!-- } 배송정보 끝 -->

                <!--교환/ 반품 정보 시작-->
                <section id="sit_ex">
                    <h2>교환/반품</h2>

                    <?php echo conv_content($default['de_change_content'], 1); ?>
                </section>
                <!--교환/ 반품 정보 끝-->
            <?php } ?>
        </div>

        <!--보여줄 페이지 REVIEW-->
        <div class="item_datas_view_contents item_datas_view_review">
            <div id="itemuse"><?php include_once(G5_SHOP_PATH.'/itemuse.php'); ?></div>
        </div>

        <!--보여줄 페이지 Q&A-->
        <div class="item_datas_view_contents item_datas_view_qna">
            <div id="itemqa"><?php include_once(G5_SHOP_PATH.'/itemqa.php'); ?></div>
        </div>
    </div>


    <?php if ($default['de_rel_list_use']) { ?>
        <!-- 관련상품 시작 { -->
        <section id="sit_rel">
            <h2>관련상품</h2>
            <?php
            $rel_skin_file = $skin_dir.'/'.$default['de_rel_list_skin'];
            if(!is_file($rel_skin_file))
                $rel_skin_file = G5_SHOP_SKIN_PATH.'/'.$default['de_rel_list_skin'];

            $sql = " select b.* from {$g5['g5_shop_item_relation_table']} a left join {$g5['g5_shop_item_table']} b on (a.it_id2=b.it_id) where a.it_id = '{$it['it_id']}' and b.it_use='1' ";
            $list = new item_list($rel_skin_file, $default['de_rel_list_mod'], 0, $default['de_rel_img_width'], $default['de_rel_img_height']);
            $list->set_query($sql);
            echo $list->run();
            ?>
        </section>
        <!-- } 관련상품 끝 -->
    <?php } ?>
</div>





<script>
$(window).on("load", function() {
    $("#sit_inf_explan").viewimageresize2();
    $('#item_datas_menu_btn1').css({'color':'#444','background-color':'#fff','border-bottom':'0px'});
    $('.item_datas_view_detail').show();
});

    $('#item_datas_menu_btn1').on('click', function(){
        $('.item_datas_menu_btn').css({'color':'#000000','background-color':'#f1f1f1'});
        $(this).css({'color':'#444','background-color':'#fff','border-bottom':'0px'});

        $('.item_datas_view_contents').hide();
        $('.item_datas_view_detail').show();
    });

    $('#item_datas_menu_btn2').on('click', function(){
        $('.item_datas_menu_btn').css({'color':'#000000','background-color':'#f1f1f1'});
        $(this).css({'color':'#444','background-color':'#fff','border-bottom':'0px'});

        $('.item_datas_view_contents').hide();
        $('.item_datas_view_info').show();
    });

    $('#item_datas_menu_btn3').on('click', function(){
        $('.item_datas_menu_btn').css({'color':'#000000','background-color':'#f1f1f1'});
        $(this).css({'color':'#444','background-color':'#fff','border-bottom':'0px'});

        $('.item_datas_view_contents').hide();
        $('.item_datas_view_review').show();
    });

    $('#item_datas_menu_btn4').on('click', function(){
        $('.item_datas_menu_btn').css({'color':'#000000','background-color':'#f1f1f1'});
        $(this).css({'color':'#444','background-color':'#fff','border-bottom':'0px'});

        $('.item_datas_view_contents').hide();
        $('.item_datas_view_qna').show();
    });
</script>