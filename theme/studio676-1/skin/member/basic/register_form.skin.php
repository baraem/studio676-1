<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_THEME_CSS_URL.'/member.css">', 0);
//연락처 나누기
$mb_hp = explode('-',$member['mb_hp']);

//이메일 나누기
$mb_email = explode('@',$member['mb_email']);

$sql = "SELECT COUNT(*) as cnt FROM BRI_member_social_profiles WHERE mb_id = '".$member['mb_id']."' ";
$social_cnt = sql_fetch($sql);
$social_cnt = $social_cnt['cnt'];
?>
<style>
    #headView {
        display: none;
    }
    .icons {
        display: none;
    }
</style>
<!--연락처, 이메일 selected-->
<? if($w == 'u'){ ?>
    <script>
        //연락처, 이메일 selected
        $( document ).ready( function() {
            $('select[name=mb_hp-01]').val('<?=$mb_hp[0]?>').attr('selected', 'selected');
            $('select[name=selectEmail]').val('<?=$mb_email[1]?>').attr('selected', 'selected');
        })
    </script>
<? } ?>

<!-- 회원정보 입력/수정 시작 { -->

<script src="<?php echo G5_JS_URL ?>/jquery.register_form.js"></script>

<?
    if(G5_IS_MOBILE)
    {?>
        <style>
            body{height: 100vh;overflow-x:  scroll;}
        </style>
    <?}
?>

<?php if($config['cf_cert_use'] && ($config['cf_cert_ipin'] || $config['cf_cert_hp'])) { ?>
    <script src="<?php echo G5_JS_URL ?>/certify.js?v=<?php echo G5_JS_VER; ?>"></script>
<?php } ?>

<!--회원가입 추가-->
<form class="form-horizontal register-form" role="form" id="fregisterform" name="fregisterform" action="<?php echo $register_action_url ?>" onsubmit="return fregisterform_submit(this);" method="post" enctype="multipart/form-data" autocomplete="off">
    <? if($w != 'u'){ ?>
        <input type="hidden" name="mb_id_chk" value="">
    <? }else{ ?>
<!--        <input type="hidden" name="mb_id_chk" value="--><?//=$member['mb_id']?><!--">-->
        <input type="hidden" name="mb_id_chk" value="12">
    <? } ?>
    <input type="hidden" name="w" value="<?php echo $w ?>">
    <input type="hidden" name="url" value="<?php echo $urlencode ?>">
    <input type="hidden" name="pim" value="<?php echo $pim;?>">
    <input type="hidden" name="agree" value="<?php echo $agree ?>">
    <input type="hidden" name="agree2" value="<?php echo $agree2 ?>">
    <input type="hidden" name="cert_type" value="<?php echo $member['mb_certify']; ?>">
    <input type="hidden" name="cert_no" value="">
    <?php if (isset($member['mb_sex'])) {  ?><input type="hidden" name="mb_sex" value="<?php echo $member['mb_sex'] ?>"><?php }  ?>
    <?php if (isset($member['mb_nick_date']) && $member['mb_nick_date'] > date("Y-m-d", G5_SERVER_TIME - ($config['cf_nick_modify'] * 86400))) { // 닉네임수정일이 지나지 않았다면  ?>
        <input type="hidden" name="mb_nick_default" value="<?php echo get_text($member['mb_nick']) ?>">
        <input type="hidden" name="mb_nick" value="<?php echo get_text($member['mb_nick']) ?>">
    <?php }  ?>
    <input type="hidden" name="mb_hp" value="">
    <input type="hidden" name="mb_email" id="mb_email" value="">
    <?php if($social_cnt){?>
        <input type="hidden" name="mb_id" value="<?php echo $member['mb_id'] ?>">
    <?}?>

    <?
        if($member['mb_id'])
        {?>
            <h4 class="tlt">회원정보수정</h4>
        <?}
        else
        {?>
            <h4 class="tlt">회원가입</h4>
        <?}
    ?>

    <?
        if(!$member['mb_id'])
        {?>
            <p class="e02">676-1 STUDIO 회원이 되시면 보다 다양한 혜택과 서비스를 이용하실 수 있습니다.</p>
        <?}
    ?>
    <div class="signup_wrap">
        <?
            if(!$member['mb_id'])
            {?>
                <ul class="signup_steps">
                    <li><!--i class="fa fa-file-text"></i--> 약관동의</li>
                    <li class="on"><!--i class="fa fa-pencil"></i--> 사용자 정보입력</li>
                    <li><!--i class="fa fa-check-circle"></i--> 회원가입 완료</li>
                </ul>
            <?}
        ?>
        <ul class="singup">
            <?php if(!$social_cnt){?>
                <li>
                    <div class="th">아이디</div>
                    <div class="td">
                        <input type="text" name="mb_id" value="<?php echo $member['mb_id'] ?>" minlength="3" maxlength="20" <?php echo $required ?> <?php echo $readonly ?> class="input08 input00">
                        <? if($w != 'u'){ ?>
                            <a href="javascript:id_chk()" class="btn_m_02">중복체크</a>
                            <!--em class="e1">* 영문자, 숫자, _ 만 입력 가능</em-->
                        <? } ?>
                    </div>
                </li>
                <li>
                    <div class="th">비밀번호</div>
                    <div class="td">
                        <input type="password" class="input09 input00" name="mb_password" id="reg_mb_password" <?php echo $required ?> minlength="3" maxlength="20">
                    </div>
                </li>
                <li>
                    <div class="th">비밀번호 확인</div>
                    <div class="td">
                        <input type="password" class="input09 input00" name="mb_password_re" id="reg_mb_password_re" <?php echo $required ?> minlength="3" maxlength="20">
                    </div>
                </li>
            <?}?>
            <li>
                <div class="th">이름</div>
                <div class="td">
                    <input type="text" class="input01 input00" id="reg_mb_name" name="mb_name" value="<?php echo get_text($member['mb_name']) ?>" <?php echo $required ?> <?php echo $readonly; ?>>
                </div>
            </li>
            <?php if ($req_nick) {  ?>
                <li>
                    <div class="th">닉네임</div>
                    <div class="td">
                        <input type="hidden" name="mb_nick_default" value="<?php echo isset($member['mb_nick']) ? get_text($member['mb_nick']) : ''; ?>">
                        <input type="text" class="input01 input00" name="mb_nick" value="<?php echo isset($member['mb_nick']) ? get_text($member['mb_nick']) : ''; ?>" id="reg_mb_nick" required maxlength="20">
                        <em class="e1">* 공백없이 한글,영문,숫자만 입력 가능 (한글2자, 영문4자 이상)</em>
                    </div>
                </li>
            <?php }  ?>
            <li class="dd">
                <div class="th">주소</div>
                <div class="td">
                    <input type="text" class="input03 input00" id="sample3_postcode" required name="mb_zip" value="<?=$member['mb_zip1']?><?=$member['mb_zip2']?>">
                    <a href="javascript:sample3_execDaumPostcode()" class="btn_m_02">주소검색</a>
                    <div id="wrap" style="display:none;border:1px solid;max-width:300px;height:300px;margin:5px 0;position:relative">
                        <img src="//t1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode()" alt="접기 버튼">
                    </div>
                    <input type="text" class="input01 input01_1 input00" id="sample3_address" required name="mb_addr1" value="<?=$member['mb_addr1']?>">
                    <input type="text" class="input01 input01_1 input00" id="sample6_address2" placeholder="상세주소" name="mb_addr2" value="<?=$member['mb_addr2']?>">
                </div>
            </li>
            <li class="ll">
                <div class="th">연락처</div>
                <div class="td">

                    <!--핸드폰 셀렉트 박스-->
                    <select class="txt" name="mb_hp-01" id="mb_hp_01">
                        <option>선택하세요</option>
                        <option value="010">010</option>
                        <option value="011">011</option>
                        <option value="016">016</option>
                        <option value="017">017</option>
                        <option value="018">018</option>
                        <option value="019">019</option>
                    </select>
                    &nbsp;-
                    <input type="text" class="input02 input00" name="mb_hp-02" required maxlength="4" value="<?=$mb_hp[1]?>"> -
                    <input type="text" class="input02 input00" name="mb_hp-03" required maxlength="4" value="<?=$mb_hp[2]?>">
                    <em class="e2">
                        <input type="checkbox" name="mb_sms" value="1" checked>
                        <b>SMS 수신 동의</b>
                        본사에서 제공하는 이벤트 및 정보를 SMS로 받아보시겠습니까?
                    </em>
                </div>
            </li>
            <li>
                <div class="th">이메일</div>
                <div class="td">
                    <input type="text" class="input05 input00" id="str_email01" required name="mb_email-01" value="<?=$mb_email[0]?>">
                    <b>@</b>
                    <input type="text" class="input06 input00" id="str_email02" required name="mb_email-02" value="<?=$mb_email[1]?>">

                    <!--email select box-->
                    <select name="selectEmail" id="selectEmail" class="txt">
                        <option value="1" selected>직접입력</option>
                        <option value="naver.com">naver.com</option>
                        <option value="hanmail.net">hanmail.net</option>
                        <option value="hotmail.com">hotmail.com</option>
                        <option value="nate.com">nate.com</option>
                        <option value="yahoo.co.kr">yahoo.co.kr</option>
                        <option value="empas.com">empas.com</option>
                        <option value="dreamwiz.com">dreamwiz.com</option>
                        <option value="freechal.com">freechal.com</option>
                        <option value="lycos.co.kr">lycos.co.kr</option>
                        <option value="korea.com">korea.com</option>
                        <option value="gmail.com">gmail.com</option>
                        <option value="hanmir.com">hanmir.com</option>
                        <option value="paran.com">paran.com</option>
                    </select>
                    <em class="e2">
                        <input type="checkbox" name="mb_mailling" value="1" checked>
                        <b>이메일 수신 동의</b>
                        본사에서 제공하는 이벤트 및 정보를 메일로 받아보시겠습니까?
                    </em>
                </div>
            </li>

            <!--추천인 아이디-->
            <!--li>
                <?php if ($w == "" && $config['cf_use_recommend']) { ?>
                    <div class="th">추천인 아이디</div>
                    <div class="td">
                        <input type="text" name="mb_recommend" id="reg_mb_recommend" class="frm_input">
                    </div>
                <?php } ?>
            </li-->
        </ul>
    </div>

    <div class="submit_wrap" style="padding-bottom: 22px">
        <input type="submit" value="<?php echo $w==''?'가입신청':'정보수정'; ?>" class="btn_m_01" accesskey="s">
        <a href="<?php echo G5_URL ?>" class="btn_m_05">취소</a>
    </div>
</form>
<!--회원가입 추가 -->

<script>
    $(function() {
        $("#reg_zip_find").css("display", "inline-block");

        <?php if($config['cf_cert_use'] && $config['cf_cert_ipin']) { ?>
        // 아이핀인증
        $("#win_ipin_cert").click(function() {
            if(!cert_confirm())
                return false;

            var url = "<?php echo G5_OKNAME_URL; ?>/ipin1.php";
            certify_win_open('kcb-ipin', url);
            return;
        });

        <?php } ?>
        <?php if($config['cf_cert_use'] && $config['cf_cert_hp']) { ?>
        // 휴대폰인증
        $("#win_hp_cert").click(function() {
            if(!cert_confirm())
                return false;

            <?php
            switch($config['cf_cert_hp']) {
                case 'kcb':
                    $cert_url = G5_OKNAME_URL.'/hpcert1.php';
                    $cert_type = 'kcb-hp';
                    break;
                case 'kcp':
                    $cert_url = G5_KCPCERT_URL.'/kcpcert_form.php';
                    $cert_type = 'kcp-hp';
                    break;
                case 'lg':
                    $cert_url = G5_LGXPAY_URL.'/AuthOnlyReq.php';
                    $cert_type = 'lg-hp';
                    break;
                default:
                    echo 'alert("기본환경설정에서 휴대폰 본인확인 설정을 해주십시오");';
                    echo 'return false;';
                    break;
            }
            ?>

            certify_win_open("<?php echo $cert_type; ?>", "<?php echo $cert_url; ?>");
            return;
        });
        <?php } ?>
    });

    // submit 최종 폼체크
    function fregisterform_submit(f)
    {

        //연락처 합치기
        $('input[name=mb_hp]').val($('select[name=mb_hp-01]').val()+$('input[name=mb_hp-02]').val()+$('input[name=mb_hp-03]').val());

        //E-mail합치기
        var mb_email = $('#str_email01').val() + '@' + $('#str_email02').val();
        $('input[name=mb_email]').attr('value',mb_email);

        //아이디 정규식 검사
        var idVal = $('input[name=mb_id]').val();
        var id_regExp = /[^0-9a-z_]+/i;

        if (idVal.match(id_regExp) != null) {
            alert('회원아이디는 영문자, 숫자, _ 만 입력하세요.');
            return false;
        }

        // if($('#fregisterform input[name=mb_id_chk]').val().trim()  != $('#fregisterform input[name=mb_id]').val().trim() ){
        //     alert("아이디 중복확인 하세요");
        //     return false;
        // }

        if (f.w.value == "") {
            if (f.mb_password.value.length < 3) {
                alert("비밀번호를 3글자 이상 입력하십시오.");
                f.mb_password.focus();
                return false;
            }
        }

        if (f.mb_password.value != f.mb_password_re.value) {
            alert("비밀번호가 같지 않습니다.");
            f.mb_password_re.focus();
            return false;
        }

        if (f.mb_password.value.length > 0) {
            if (f.mb_password_re.value.length < 3) {
                alert("비밀번호를 3글자 이상 입력하십시오.");
                f.mb_password_re.focus();
                return false;
            }
        }

        // 이름 검사
        if (f.w.value=="") {
            if (f.mb_name.value.length < 1) {
                alert("이름을 입력하십시오.");
                f.mb_name.focus();
                return false;
            }

            /*
            var pattern = /([^가-힣\x20])/i;
            if (pattern.test(f.mb_name.value)) {
                alert("이름은 한글로 입력하십시오.");
                f.mb_name.select();
                return false;
            }
            */
        }

        <?php if($w == '' && $config['cf_cert_use'] && $config['cf_cert_req']) { ?>
        // 본인확인 체크
        if(f.cert_no.value=="") {
            alert("회원가입을 위해서는 본인확인을 해주셔야 합니다.");
            return false;
        }
        <?php } ?>

        // 닉네임 검사
        if ((f.w.value == "") || (f.w.value == "u" && f.mb_nick.defaultValue != f.mb_nick.value)) {
            var msg = reg_mb_nick_check();
            if (msg) {
                alert(msg);
                f.reg_mb_nick.select();
                return false;
            }
        }

        //E-mail 정규식 검사
        var emailVal = $('input[name=mb_email]').val();
        var email_regExp = /([0-9a-zA-Z_-]+)@([0-9a-zA-Z_-]+)\.([0-9a-zA-Z_-]+)/;

        if (emailVal.match(email_regExp) == null) {
            alert('E-mail 주소가 형식에 맞지 않습니다.');
            return false;
        }

        //연락처 정규식 검사
        var hpVal = $('input[name=mb_hp]').val();
        var hp_regExp = /^01[0-9]{8,9}$/;

        if (hpVal.match(hp_regExp) == null) {
            alert('연락처를 올바르게 입력해 주십시오.');
            return false;
        }

        <?php if (($config['cf_use_hp'] || $config['cf_cert_hp']) && $config['cf_req_hp']) {  ?>
        // 휴대폰번호 체크
        // var msg = reg_mb_hp_check();
        // if (msg) {
        //     alert(msg);
        //     f.reg_mb_hp.select();
        //     return false;
        // }
        <?php } ?>

        if (typeof f.mb_icon != "undefined") {
            if (f.mb_icon.value) {
                if (!f.mb_icon.value.toLowerCase().match(/.(gif|jpe?g|png)$/i)) {
                    alert("회원아이콘이 이미지 파일이 아닙니다.");
                    f.mb_icon.focus();
                    return false;
                }
            }
        }

        if (typeof f.mb_img != "undefined") {
            if (f.mb_img.value) {
                if (!f.mb_img.value.toLowerCase().match(/.(gif|jpe?g|png)$/i)) {
                    alert("회원이미지가 이미지 파일이 아닙니다.");
                    f.mb_img.focus();
                    return false;
                }
            }
        }

        if (typeof(f.mb_recommend) != "undefined" && f.mb_recommend.value) {
            if (f.mb_id.value == f.mb_recommend.value) {
                alert("본인을 추천할 수 없습니다.");
                f.mb_recommend.focus();
                return false;
            }

            var msg = reg_mb_recommend_check();
            if (msg) {
                alert(msg);
                f.mb_recommend.select();
                return false;
            }
        }

        document.getElementById("btn_submit").disabled = "disabled";

        return true;
    }

</script>

<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
    //이메일 입력방식 선택
    $('#selectEmail').change(function(){
        $("#selectEmail option:selected").each(function () {
            if($(this).val()== '1'){
                //직접입력일 경우
                $("#str_email02").val(''); //값 초기화
                $("#str_email02").attr("disabled",false); //활성화
            }else{ //직접입력이 아닐경우
                $("#str_email02").val($(this).text()); //선택값 입력
                $("#str_email02").attr("disabled",true); //비활성화
            }
        });
    });


    // 우편번호 찾기 찾기 화면을 넣을 element
    var element_wrap = document.getElementById('wrap');

    function foldDaumPostcode() {
        // iframe을 넣은 element를 안보이게 한다.
        element_wrap.style.display = 'none';
    }

    function sample3_execDaumPostcode() {
        // 현재 scroll 위치를 저장해놓는다.
        var currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
        new daum.Postcode({
            oncomplete: function(data) {
                // 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullAddr = data.address; // 최종 주소 변수
                var extraAddr = ''; // 조합형 주소 변수

                // 기본 주소가 도로명 타입일때 조합한다.
                if(data.addressType === 'R'){
                    //법정동명이 있을 경우 추가한다.
                    if(data.bname !== ''){
                        extraAddr += data.bname;
                    }
                    // 건물명이 있을 경우 추가한다.
                    if(data.buildingName !== ''){
                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                    }
                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById('sample3_postcode').value = data.zonecode; //5자리 새우편번호 사용
                document.getElementById('sample3_address').value = fullAddr;

                // 커서를 상세주소 필드로 이동한다.
                document.getElementById('sample6_address2').focus();

                // iframe을 넣은 element를 안보이게 한다.
                // (autoClose:false 기능을 이용한다면, 아래 코드를 제거해야 화면에서 사라지지 않는다.)
                element_wrap.style.display = 'none';

                // 우편번호 찾기 화면이 보이기 이전으로 scroll 위치를 되돌린다.
                document.body.scrollTop = currentScroll;
            },
            // 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
            onresize : function(size) {
                element_wrap.style.height = size.height+'px';
            },
            width : '100%',
            height : '100%'
        }).embed(element_wrap);

        // iframe을 넣은 element를 보이게 한다.
        element_wrap.style.display = 'block';
    }

    function id_chk() {
        $data = $('input[name=mb_id]').val();

        var id_regExp = /[^0-9a-z_]+/i;

        if($data == ""){
            alert("아이디를 입력하세요.")
            return false;
        }

        if ($data.match(id_regExp) != null) {
            alert('회원아이디는 영문자, 숫자, _ 만 입력하세요.');
            return;
        }else{
            $.ajax({
                url:"<?=G5_THEME_URL?>/ajax/id_chk_ajax.php?data="+$data,
                type:'GET',
                dataType: 'html',
                success:function(data){
                    if(data == 0){
                        alert('사용 중인 아이디입니다.');
                        $('input[name=mb_id_chk]').val('0');
                    }else{
                        alert('사용 가능한 아이디입니다.');
                        $('input[name=mb_id_chk]').val(data);
                    }

                },
                error:function(jqXHR, textStatus, errorThrown){
                    alert("에러 발생~~ \n" + textStatus + " : " + errorThrown);
                    self.close();
                }
            });
        }

    }
</script>

<!-- } 회원정보 입력/수정 끝 -->