<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$member_skin_url.'/style.css">', 0);
include_once (G5_THEME_SHOP_PATH.'/shop.head.php');

?>
<style>
    div, input {
        font-family: Noto Sans;
    }

    #login_main_div1{
        width: 300px;
        margin: 0 auto;
    }

    .login_main_div2{
        width: 100%;
        text-align: center;
        margin: 0 auto;
    }

    .login_main_h1{
        font-family: Karla Bord;
        font-size: 18px;
        margin-top: 18px;
    }

    .login_input_text{
        width: 100%;
        height: 40px;
        padding-left: 10px;
        font-size: 12px;
    }

    .login_btn_con{
        margin-top: 6px;
        width: 100%;
        height: 40px;
    }

    .login_btn_sub1{
        width: 100%;
        height: 100%;
        border: none;
        background-color: #000000;
        color: #ffffff;
    }

    .login_sub_con{
        margin-top: 6px;
    }

    .login_sub_btnCon{
        display: inline-block;
        width: 49.6%;
        height: 40px;
        background-color: #eeeeee;
        line-height: 40px;
    }

    .login_join{
        float: left;
    }

    .login_find{
        float: right;
    }
</style>
<!-- 로그인 시작 { -->
<div id="login_main_div1">

    <div class="login_main_div2">
        <h1 class="login_main_h1">LOGIN</h1>

        <div class="login_main_div3">
            <form name="flogin" action="<?php echo $login_action_url ?>" onsubmit="return flogin_submit(this);" method="post">
                <input type="hidden" name="url" value="<?php echo $login_url ?>">

                <fieldset id="login_fs">
                    <div>
                        <div style="margin-top: 24px;">
                            <input type="text" name="mb_id" id="login_id" required class="login_input_text" placeholder="아이디">
                        </div>
                        <div style="margin-top: 5px;">
                            <input type="password" name="mb_password" id="login_pw" required class="login_input_text" placeholder="비밀번호">
                        </div>
                    </div>
                    <div class="login_btn_con">
                        <input type="submit" value="로그인" class="login_btn_sub1">
                    </div>
                </fieldset>
            </form>

            <div class="login_sub_con">
                <div class="login_sub_btnCon login_join">
                    <a href="./register.php" class="login_btn_a">회원가입</a>
                </div>

                <div class="login_sub_btnCon login_find">
                    <a href="<?php echo G5_BBS_URL ?>/password_lost.php" class="login_btn_btn1">정보찾기</a>
                </div>
            </div>
        </div>
    </div>

    <?php
    // 소셜로그인 사용시 소셜로그인 버튼
    @include_once(get_social_skin_path().'/social_register.skin.php');
    ?>

    <?php // 쇼핑몰 사용시 여기부터 ?>
    <?php if ($default['de_level_sell'] == 1) { // 상품구입 권한 ?>

        <!-- 주문하기, 신청하기 -->
        <?php if (preg_match("/orderform.php/", $url)) { ?>

            <section id="mb_login_notmb" class="mbskin">
                <h2>비회원 구매</h2>

                <p>
                    비회원으로 주문하시는 경우 포인트는 지급하지 않습니다.
                </p>

                <div id="guest_privacy">
                    <?php echo $default['de_guest_privacy']; ?>
                </div>

                <label for="agree">개인정보수집에 대한 내용을 읽었으며 이에 동의합니다.</label>
                <input type="checkbox" id="agree" value="1">

                <div class="btn_confirm">
                    <a href="javascript:guest_submit(document.flogin);" class="btn_submit">비회원으로 구매하기</a>
                </div>

                <script>
                    function guest_submit(f)
                    {
                        if (document.getElementById('agree')) {
                            if (!document.getElementById('agree').checked) {
                                alert("개인정보수집에 대한 내용을 읽고 이에 동의하셔야 합니다.");
                                return;
                            }
                        }

                        f.url.value = "<?php echo $url; ?>";
                        f.action = "<?php echo $url; ?>";
                        f.submit();
                    }
                </script>
            </section>

        <?php } else if (preg_match("/orderinquiry.php$/", $url)) { ?>
            <div class="mbskin" id="mb_login_od_wr" style="margin-top: 100px">
                <h2>비회원 주문조회 </h2>

                <fieldset id="mb_login_od">
                    <legend>비회원 주문조회</legend>

                    <form name="forderinquiry" method="post" action="<?php echo urldecode($url); ?>" autocomplete="off">

                        <label for="od_id" class="od_id sound_only">주문서번호<strong class="sound_only"> 필수</strong></label>
                        <input type="text" name="od_id" value="<?php echo $od_id; ?>" id="od_id" required class="frm_input required" size="20" placeholder="주문서번호">
                        <label for="id_pwd" class="od_pwd sound_only" >비밀번호<strong class="sound_only"> 필수</strong></label>
                        <input type="password" name="od_pwd" size="20" id="od_pwd" required class="frm_input required" placeholder="비밀번호">
                        <input type="submit" value="확인" class="btn_submit">

                    </form>
                </fieldset>

                <section id="mb_login_odinfo">
                    <p>메일로 발송해드린 주문서의 <strong>주문번호</strong> 및 주문 시 입력하신 <strong>비밀번호</strong>를 정확히 입력해주십시오.</p>
                </section>

            </div>
        <?php } ?>

    <?php } ?>
    <?php // 쇼핑몰 사용시 여기까지 반드시 복사해 넣으세요 ?>

    </form>

</div>

<!-- } 로그인 끝 -->
