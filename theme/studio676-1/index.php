<?php
define('_INDEX_', true);
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if (G5_IS_MOBILE) {
    include_once(G5_THEME_MOBILE_PATH.'/index.php');
    return;
}

if(G5_COMMUNITY_USE === false) {
    include_once(G5_THEME_SHOP_PATH.'/index.php');
    return;
}

include_once(G5_THEME_SHOP_PATH.'/shop.head.php');

?>

<style>
    #wrapper {
        padding-top : 0 !important;
    }
</style>

<script>
    $(".mainSlider ul").bxSlider({
        mode : "fade",
        auto : true,
        pause : 7000,
        speed : 1500
    });
</script>


<!--<//? include_once (G5_THEME_PATH."/cont_menu.php"); ?>-->

<!--new item-->
<!--<div class="shop_title main_tlt">
    <a href="<?/*=G5_SHOP_URL*/?>/list.php?ca_id=10">WOMEN</a>
</div>
<?php
/*$ca_id = 10;
$list = new item_list();
$list->set_category($ca_id, 1);
$list->set_category($ca_id, 2);
$list->set_category($ca_id, 3);
$list->set_list_mod(2);
$list->set_list_row(4);
$list->set_is_page(true);
$list->set_list_skin(G5_SHOP_SKIN_PATH.'/list.1.skin.php');
$list->set_view('it_img', true);
$list->set_view('it_id', false);
$list->set_view('it_name', true);
$list->set_view('it_basic', true);
$list->set_view('it_cust_price', true);
$list->set_view('it_price', true);
echo $list->run();

// where 된 전체 상품수
$total_count = $list->total_count;

// 전체 페이지 계산
if($total_count != 0){
    $total_page  = ceil($total_count / $total_count);
    */?>
    <div class="loading" style="display: none"><img src="<?/*=G5_THEME_URL*/?>/img/bri/loading.gif" alt="로딩"/> </div>
    <div class="plus_list20 plus_list" onclick="plus_list(item20, <?/*=$ca_id*/?>)" style="cursor: pointer">MORE()</div>
    --><?/*
}
*/?>

<!--<script>

    var item20 = 2;
    var total_count20 = Math.ceil(<?/*=$total_count*/?> / 8);
    $(function(){
        $('.plus_list20').text('MORE( 1/' + total_count20 + ')');
    });
    $('.plus_list20').on('click',function(){

        if( total_count20 >= item20 )
        {
            $(this).text('MORE(' + item20 + '/' + total_count20 + ')');
            item20 +=1;
        }

    });

</script>-->


<!--공동구매-->
<div class="shop_title main_tlt">
    <a href="<?=G5_SHOP_URL?>/list.php?ca_id=a0">공동구매</a>
</div>
<?php
$ca_id = "c0";
$list = new item_list();
$list->set_category($ca_id, 1);
$list->set_category($ca_id, 2);
$list->set_category($ca_id, 3);
$list->set_list_mod(3);
$list->set_list_row(2);
$list->set_is_page(true);
$list->set_list_skin(G5_SHOP_SKIN_PATH.'/list.1.skin.php');
$list->set_view('it_img', true);
$list->set_view('it_id', false);
$list->set_view('it_name', true);
$list->set_view('it_basic', true);
$list->set_view('it_cust_price', true);
$list->set_view('it_price', true);
echo $list->run();

// where 된 전체 상품수
$total_count = $list->total_count;

// 전체 페이지 계산
if($total_count != 0){
    $total_page  = ceil($total_count / $total_count);
    ?>
    <div class="loading" style="display: none"><img src="<?=G5_THEME_URL?>/img/bri/loading.gif" alt="로딩"/> </div>
    <div class="plus_list90 plus_list" onclick="plus_list(item100, '<?=$ca_id?>')" style="cursor: pointer">MORE()</div>
    <?
}

?>

<script>

    var item100 = 2;
    var total_count90 = Math.ceil(<?=$total_count?> / 6);
    $(function(){
        $('.plus_list90').text('MORE( 1/' + total_count90 + ')');
    });
    $('.plus_list90').on('click',function(){

        if( total_count90 >= item90 )
        {
            $(this).text('MORE(' + item90 + '/' + total_count90 + ')');
            item90 +=1;
        }

    });

</script>



<!--신발-->
<!--
<div class="shop_title main_tlt">
    <a href="<//?=G5_SHOP_URL?>/list.php?ca_id=a0">SHOES</a>
</div>
<//?php
$ca_id = "a010";
$list = new item_list();
$list->set_category($ca_id, 1);
$list->set_category($ca_id, 2);
$list->set_category($ca_id, 3);
$list->set_list_mod(3);
$list->set_list_row(2);
$list->set_is_page(true);
$list->set_list_skin(G5_SHOP_SKIN_PATH.'/list.1.skin.php');
$list->set_view('it_img', true);
$list->set_view('it_id', false);
$list->set_view('it_name', true);
$list->set_view('it_basic', true);
$list->set_view('it_cust_price', true);
$list->set_view('it_price', true);
echo $list->run();

// where 된 전체 상품수
$total_count = $list->total_count;

// 전체 페이지 계산
if($total_count != 0){
    $total_page  = ceil($total_count / $total_count);
?>
    <div class="loading" style="display: none"><img src="<//?=G5_THEME_URL?>/img/bri/loading.gif" alt="로딩"/> </div>
    <div class="plus_list90 plus_list" onclick="plus_list(item90, '<//?=$ca_id?>')" style="cursor: pointer">MORE()</div>
<//?
}

?>

<script>

    var item90 = 2;
    var total_count90 = Math.ceil(<?=$total_count?> / 6);
    $(function(){
        $('.plus_list90').text('MORE( 1/' + total_count90 + ')');
    });
    $('.plus_list90').on('click',function(){

        if( total_count90 >= item90 )
        {
            $(this).text('MORE(' + item90 + '/' + total_count90 + ')');
            item90 +=1;
        }

    });

</script>
-->

<!--가방-->
<!--
<div class="shop_title main_tlt">
    <a href="<//?=G5_SHOP_URL?>/list.php?ca_id=a020">BAG</a>
</div>
<//?php
$ca_id = "a020";
$list = new item_list();
$list->set_category($ca_id, 1);
$list->set_category($ca_id, 2);
$list->set_category($ca_id, 3);
$list->set_list_mod(3);
$list->set_list_row(2);
$list->set_is_page(true);
$list->set_list_skin(G5_SHOP_SKIN_PATH.'/list.1.skin.php');
$list->set_view('it_img', true);
$list->set_view('it_id', false);
$list->set_view('it_name', true);
$list->set_view('it_basic', true);
$list->set_view('it_cust_price', true);
$list->set_view('it_price', true);
echo $list->run();

// where 된 전체 상품수
$total_count = $list->total_count;

// 전체 페이지 계산
if($total_count != 0){
    $total_page  = ceil($total_count / $total_count);
    ?>
    <div class="loading" style="display: none"><img src="<//?=G5_THEME_URL?>/img/bri/loading.gif" alt="로딩"/> </div>
    <div class="plus_list15 plus_list" onclick="plus_list(item15, '<//?=$ca_id?>')" style="cursor: pointer">MORE()</div>
<//?
}
?>



<script>

    var item15 = 2;
    var total_count15 = Math.ceil(<?=$total_count?> / 6);
    $(function(){
        $('.plus_list15').text('MORE( 1/' + total_count15 + ')');
    });
    $('.plus_list15').on('click',function(){

        if( total_count15 >= item15 )
        {
            $(this).text('MORE(' + item15 + '/' + total_count15 + ')');
            item15 +=1;
        }

    });

</script>
-->

<!--new item-->
<!--
<div class="shop_title main_tlt">
<a href="<//?=G5_SHOP_URL?></list.php?ca_id=20">ACC</a>
</div>
<//?php
$ca_id = "a030";
$list = new item_list();
$list->set_category($ca_id, 1);
$list->set_category($ca_id, 2);
$list->set_category($ca_id, 3);
$list->set_list_mod(3);
$list->set_list_row(2);
$list->set_is_page(true);
$list->set_list_skin(G5_SHOP_SKIN_PATH.'/list.1.skin.php');
$list->set_view('it_img', true);
$list->set_view('it_id', false);
$list->set_view('it_name', true);
$list->set_view('it_basic', true);
$list->set_view('it_cust_price', true);
$list->set_view('it_price', true);
echo $list->run();

// where 된 전체 상품수
$total_count = $list->total_count;

// 전체 페이지 계산
if($total_count != 0){
    $total_page  = ceil($total_count / $total_count);
    ?>
    <div class="loading" style="display: none"><img src="<//?=G5_THEME_URL?>/img/bri/loading.gif" alt="로딩"/> </div>
   <div class="plus_list30 plus_list" onclick="plus_list(item, '<//?=$ca_id?>')" style="cursor: pointer">MORE()</div>
    <//?
}
?>

<script>

    var item = 2;
    var total_count = Math.ceil(<?=$total_count?> / 6);
    $(function(){
        $('.plus_list30').text('MORE( 1/' + total_count + ')');
    });
    $('.plus_list30').on('click',function(){

        if( total_count >= item )
        {
            $(this).text('MORE(' + item + '/' + total_count + ')');
            item +=1;
        }

    });

</script>
-->

<!--new item-->
<div class="shop_title main_tlt">
    <a href="<?=G5_SHOP_URL?>/list.php?ca_id=b0">상시판매</a>
</div>
<?php
$ca_id = "b0";
$list = new item_list();
$list->set_category($ca_id, 1);
$list->set_category($ca_id, 2);
$list->set_category($ca_id, 3);
$list->set_list_mod(3);
$list->set_list_row(2);
$list->set_is_page(true);
$list->set_list_skin(G5_SHOP_SKIN_PATH.'/list.1.skin.php');
$list->set_view('it_img', true);
$list->set_view('it_id', false);
$list->set_view('it_name', true);
$list->set_view('it_basic', true);
$list->set_view('it_cust_price', true);
$list->set_view('it_price', true);
echo $list->run();

// where 된 전체 상품수
$total_count = $list->total_count;

// 전체 페이지 계산
if($total_count != 0){
    $total_page  = ceil($total_count / $total_count);
    ?>
    <div class="loading" style="display: none"><img src="<?=G5_THEME_URL?>/img/bri/loading.gif" alt="로딩"/> </div>
    <div class="plus_list10 plus_list" onclick="plus_list(item10, '<?=$ca_id?>')" style="cursor: pointer">MORE()</div>
    <?
}
?>
<script>

    var item10 = 2;
    var total_count10 = Math.ceil(<?=$total_count?> / 6);
    $(function(){

        $('.plus_list10').text('MORE( 1/' + total_count10 + ')');
    });
    $('.plus_list10').on('click',function(){

        if( total_count10 >= item10 )
        {
            $(this).text('MORE(' + item10 + '/' + total_count10 + ')');
            item10 +=1;
        }

    });

</script>

<!--new item-->
<!--<div class="shop_title main_tlt">
    <a href="<?/*=G5_SHOP_URL*/?>/list.php?ca_id=80">BRANDS</a>
</div>
<?php
/*$ca_id = 80;
$list = new item_list();
$list->set_category($ca_id, 1);
$list->set_category($ca_id, 2);
$list->set_category($ca_id, 3);
$list->set_list_mod(2);
$list->set_list_row(4);
$list->set_is_page(true);
$list->set_list_skin(G5_SHOP_SKIN_PATH.'/list.1.skin.php');
$list->set_view('it_img', true);
$list->set_view('it_id', false);
$list->set_view('it_name', true);
$list->set_view('it_basic', true);
$list->set_view('it_cust_price', true);
$list->set_view('it_price', true);
echo $list->run();

// where 된 전체 상품수
$total_count = $list->total_count;


// 전체 페이지 계산
if($total_count != 0){
    $total_page  = ceil($total_count / $total_count);
    */?>
    <div class="loading" style="display: none"><img src="<?/*=G5_THEME_URL*/?>/img/bri/loading.gif" alt="로딩"/> </div>
    <div class="plus_list80 plus_list" onclick="plus_list(item80, <?/*=$ca_id*/?>)" style="cursor: pointer">MORE()</div>
    <?/*
}
*/?>

<script>

    var item80 = 2;
    var total_count80 = Math.ceil(<?/*=$total_count*/?> / 8);
    $(function(){
        $('.plus_list80').text('MORE( 1/' + total_count80 + ')');
    })
    $('.plus_list80').on('click',function(){

        if( total_count80 >= item80 )
        {
            $(this).text('MORE(' + item80 + '/' + total_count80 + ')');
            item80 +=1;
        }

    });

</script>-->

<!--new item-->

<!--<div class="shop_title main_tlt">
    <a href="<?/*=G5_SHOP_URL*/?>/list.php?ca_id=70">FURNITURE</a>
</div>
<?php
/*$ca_id = 70;
$list = new item_list();
$list->set_category($ca_id, 1);
$list->set_category($ca_id, 2);
$list->set_category($ca_id, 3);
$list->set_list_mod(2);
$list->set_list_row(4);
$list->set_is_page(true);
$list->set_list_skin(G5_SHOP_SKIN_PATH.'/list.1.skin.php');
$list->set_view('it_img', true);
$list->set_view('it_id', false);
$list->set_view('it_name', true);
$list->set_view('it_basic', true);
$list->set_view('it_cust_price', true);
$list->set_view('it_price', true);
echo $list->run();

// where 된 전체 상품수
$total_count = $list->total_count;


// 전체 페이지 계산
if($total_count != 0){
    $total_page  =  $total_count ? ceil($total_count / $total_count) : $total_count;
    */?>
    <div class="loading" style="display: none"><img src="<?/*=G5_THEME_URL*/?>/img/bri/loading.gif" alt="로딩"/> </div>
    <div class="plus_list40 plus_list" onclick="plus_list(item, <?/*=$ca_id*/?>)" style="cursor: pointer">MORE()</div>
    --><?/*
}
*/?>

<!--<script>

    var item40 = 2;
    var total_count40 = Math.ceil(<?/*=$total_count*/?> / 8);
    $(function(){
        $('.plus_list40').text('MORE( 1/' + total_count40 + ')');
    })
    $('.plus_list40').on('click',function(){

        if( total_count40 >= item40 )
        {
            $(this).text('MORE(' + item40 + '/' + total_count40 + ')');
            item40 +=1;
        }

    });

</script>
-->

<!--new item-->

<!--<div class="shop_title main_tlt">
    <a href="<?/*=G5_SHOP_URL*/?>/list.php?ca_id=60">LIGHITING</a>
</div>
<?php
/*$ca_id = 60;
$list = new item_list();
$list->set_category($ca_id, 1);
$list->set_category($ca_id, 2);
$list->set_category($ca_id, 3);
$list->set_list_mod(2);
$list->set_list_row(4);
$list->set_is_page(true);
$list->set_list_skin(G5_SHOP_SKIN_PATH.'/list.1.skin.php');
$list->set_view('it_img', true);
$list->set_view('it_id', false);
$list->set_view('it_name', true);
$list->set_view('it_basic', true);
$list->set_view('it_cust_price', true);
$list->set_view('it_price', true);
echo $list->run();

// where 된 전체 상품수
$total_count = $list->total_count;


// 전체 페이지 계산
if($total_count != 0){
    $total_page  =  $total_count ? ceil($total_count / $total_count) : $total_count;
    */?>
    <div class="loading" style="display: none"><img src="<?/*=G5_THEME_URL*/?>/img/bri/loading.gif" alt="로딩"/> </div>
    <div class="plus_list50 plus_list" onclick="plus_list(item, <?/*=$ca_id*/?>)" style="cursor: pointer">MORE()</div>
    --><?/*
}
*/?>

<!--<script>

    var item50 = 2;
    var total_count50 = Math.ceil(<?/*=$total_count*/?> / 8);
    $(function(){
        $('.plus_list50').text('MORE( 1/' + total_count50 + ')');
    })
    $('.plus_list50').on('click',function(){

        if( total_count50 >= item50 )
        {
            $(this).text('MORE(' + item50 + '/' + total_count50 + ')');
            item50 +=1;
        }

    });

</script>-->


<?php
include_once (G5_THEME_SHOP_PATH.'/shop.tail.php');
?>
