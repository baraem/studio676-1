<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if( ! $config['cf_social_login_use']) {     //소셜 로그인을 사용하지 않으면
    return;
}

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.G5_JS_URL.'/remodal/remodal.css">', 11);
add_stylesheet('<link rel="stylesheet" href="'.G5_JS_URL.'/remodal/remodal-default-theme.css">', 12);
add_stylesheet('<link rel="stylesheet" href="'.get_social_skin_url().'/style.css">', 13);
add_javascript('<script src="'.G5_JS_URL.'/remodal/remodal.js"></script>', 10);

$email_msg = $is_exists_email ? '등록할 이메일이 중복되었습니다.다른 이메일을 입력해 주세요.' : '';
$nick_msg = $user_nick ? '등록할 닉네임이 중복되었습니다.다른 닉네임을 입력해 주세요.' : '';
?>
<!-- 회원정보 입력/수정 시작 { -->

<?php if($_GET['provider']) { ?>
    <style>
        #headView {
            display: none;
        }
    </style>
<?php } ?>


<style>

    /*개인 기업 안 버튼*/
    .regisel_selCons_btn{
        width: 100%;
        max-width: 120px;
        height: 43px;
        border: 1px solid #959595;
        color: #959595;
        font-size: 14px;
        font-weight: bold;
        margin: 20px 5px 10px 0;
        background-color: #ffffff;
    }
    .regisel_selCons_btn:hover{
        border: 1px solid #d7374a;
        background-color: #fd4157;
        color: #ffffff;
    }

    .regisel_selCons_btn_on{
        width: 100%;
        max-width: 120px;
        height: 43px;
        border: 1px solid #d7374a;
        color: #ffffff;
        font-size: 14px;
        font-weight: bold;
        margin: 20px 5px 10px 0;
        background-color: #fd4157;
    }

    .board_regif_div1{
        width: 100%;
        border-top: none;
        border-bottom: 0;
    }

    .board_ddregif_div2{border-bottom: 0;}

    .board_regif_div2{
        width: 100%;
        padding: 30px 20px 0 20px;

    }

    /*타이틀*/
    .board_regif_h2
    {
        font-size: 32px;
        text-align: center;
    }

    /*사이드맵*/
    .board_regif_sidemap{
        display: inline-block;
        float: right;
        font-size: 12px;
        color: #adadad;
    }

    /*아래빈칸..*/
    .board_regif_div_3_2{
        width: 100%;
        font-size: 12px;
        text-align: center;
        background: #f8f8f8;
        padding: 20px;
        border: 1px solid #e1e1e1;
        margin: 0 0 5px;
    }

    .red_text_div{
        height: 14px;
        margin-bottom: 20px;
    }

    .red_text_div_p_14{
        font-size: 14px;
        font-weight: bold;
        color: #fd4056;
    }

    .board_regif_input{
        width: 100%;
        max-width: 461px;
        height: 39px;
        border: 1px solid #cdcdcd;
        padding-left: 10px;
    }

    .board_regif_span2{
        line-height: 30px;
    }

    #msg_mb_nick{
        position: relative;
        left: 50%;
        transform: translate(-50%);
        top: 5px;
    }

</style>
<!-- 회원정보 입력/수정 시작 { -->
<div class="container">
    <div class="mbskin" id="register_member">

        <script src="<?php echo G5_JS_URL ?>/jquery.register_form.js"></script>

        <!-- 새로가입 시작 -->
        <form id="fregisterform" name="fregisterform" action="<?php echo $register_action_url; ?>" onsubmit="return fregisterform_submit(this);" method="post" enctype="multipart/form-data" autocomplete="off">
            <input type="hidden" name="w" value="<?php echo $w; ?>">
            <input type="hidden" name="url" value="<?php echo $urlencode; ?>">
            <input type="hidden" name="provider" value="<?php echo $provider_name;?>" >
            <input type="hidden" name="action" value="register">

            <input type="hidden" name="mb_id" value="<?php echo $user_id; ?>" id="reg_mb_id">
            <input type="hidden" name="mb_nick_default" value="<?php echo isset($user_nick)?get_text($user_nick):''; ?>">
            <input type="hidden" name="mb_nick" value="<?php echo isset($user_nick)?get_text($user_nick):''; ?>">

            <div id=""  class="board_regif_div1">
                <div class="board_ddregif_div2">
                    <div class="board_regif_div_3_2">
                        아래의 빈칸에 알맞은 내용을 기입해주세요
                    </div>

                    <div class="toggle">
                        <div class="toggle-title">
                            <span class="right_i"><i></i> 자세히보기</span>
                            <span class="title-name"><input type="checkbox" name="agree" value="1" id="agree11"> <label for="agree11">회원가입약관</label></span>
                        </div>
                        <div class="toggle-inner">
                            <p><?php echo conv_content($config['cf_stipulation'], 0); ?></p>
                        </div>
                    </div>  <!-- END OF TOGGLE -->
                    <div class="toggle">
                        <div class="toggle-title">
                            <span class="right_i"><i></i> 자세히보기</span>
                            <span class="title-name"><input type="checkbox" name="agree2" value="1" id="agree21"> <label for="agree21">개인정보처리방침안내</label></span>
                        </div>
                        <div class="toggle-inner">
                            <p><?php echo conv_content($config['cf_privacy'], 0); ?></p>
                        </div>
                    </div>  <!-- END OF TOGGLE -->
                    <div class="all_agree">
                        <span class="title-name"><input type="checkbox" name="chk_all" value="1" id="chk_all"> <label for="chk_all"><strong>전체약관에 동의합니다.</strong></label></span>
                    </div>

                    <!--                    <div>-->
                    <!--                        <button class="regisel_selCons_btn_on" onclick="#">개인회원</button>-->
                    <!--                        <button class="regisel_selCons_btn" onclick="#">기업회원</button>-->
                    <!--                    </div>-->

                    <div class="sns_tbl tbl_wrap">
                        <table>
                            <caption>개인정보 입력</caption>
                            <tbody style="display: block; padding: 30px 0">
                            <tr>
                                <th scope="row"><label for="reg_mb_email">E-mail<strong class="sound_only">필수</strong></label></th>
                                <td>
                                    <input type="text" name="mb_email" value="<?php echo isset($user_email)?$user_email:''; ?>" id="reg_mb_email" required class="frm_input email required mb_lay_02" maxlength="100" placeholder="이메일을 입력해주세요." >
                                    <p class="email_msg"><?php echo $email_msg; ?></p>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="reg_mb_name">이름<strong class="sound_only">필수</strong></label></th>
                                <td>
                                    <input type="text" name="mb_name" value="<?php echo isset($user_name)?$user_name:''; ?>" id="reg_mb_name" required class="frm_input required mb_lay_02" maxlength="100" placeholder="이름을 입력해주세요." >
                                </td>
                            </tr>
<!--                            <tr>-->
<!--                                <th scope="row"><label for="reg_mb_email">별명<strong class="sound_only">필수</strong></label></th>-->
<!--                                <td>-->
<!--                                    --><?php //if ($req_nick) {  ?>
<!--                                        <label for="reg_mb_nick" class="sound_only">별명<strong>필수</strong></label>-->
<!--                                    --><?php //}  ?>
<!--                                    <input type="text" name="mb_nick" value="--><?php //echo isset($user_nick)?get_text($user_nick):''; ?><!--" id="reg_mb_nick" required class="frm_input required mb_lay_02" size="70" maxlength="100" placeholder="닉네임을 입력해주세요." >-->
<!--                                    <p class="email_msg">--><?php //echo $nick_msg; ?><!--</p>-->
<!--                                    <span class="board_regif_span2">-->
<!--                                        *공백없이 한글, 영문, 숫자만 입력가능(--><?php //echo (int)$config['cf_nick_modify'] ?><!--일 이내에는 변경 할 수 없음.)-->
<!--                                    </span>-->
<!--                                </td>-->
<!--                            </tr>-->
                            <!--                            <tr>-->
                            <!--                                <th scope="row"><label for="reg_mb_email">연락처<strong class="sound_only">필수</strong></label></th>-->
                            <!--                                <td>-->
                            <!--                                    <select class="mb_hp-01 frm_input mb_lay_04" style="float: left" name="mb_hp-01">-->
                            <!--                                        <option value="010">010</option>-->
                            <!--                                        <option value="011">011</option>-->
                            <!--                                        <option value="016">016</option>-->
                            <!--                                        <option value="017">017</option>-->
                            <!--                                        <option value="018">018</option>-->
                            <!--                                        <option value="019">019</option>-->
                            <!--                                    </select>&nbsp;--->
                            <!--                                    <input type="text" class="mb_hp-02 frm_input mb_lay_03" name="mb_hp-02" required maxlength="4" value="--><?//=$mb_hp[1]?><!--"> --->
                            <!--                                    <input type="text" class="mb_hp-03 frm_input mb_lay_03" name="mb_hp-03" required maxlength="4" value="--><?//=$mb_hp[2]?><!--">-->
                            <!--                                </td>-->
                            <!--                            </tr>-->

                            </tbody>
                        </table>
                    </div>

                    <div class="btn_confirm">
                        <input type="submit" value="회원가입" id="btn_submit" class="btn_submit" accesskey="s">
                        <a href="<?php echo G5_URL ?>" class="btn_cancel">취소</a>
                    </div>
        </form>
        <!-- 새로가입 끝 -->


        <script>

            // submit 최종 폼체크
            function fregisterform_submit(f)
            {

                if (!f.agree.checked) {
                    alert("회원가입약관의 내용에 동의하셔야 회원가입 하실 수 있습니다.");
                    f.agree.focus();
                    return false;
                }

                if (!f.agree2.checked) {
                    alert("개인정보처리방침안내의 내용에 동의하셔야 회원가입 하실 수 있습니다.");
                    f.agree2.focus();
                    return false;
                }

                // E-mail 검사
                if ((f.w.value == "") || (f.w.value == "u" && f.mb_email.defaultValue != f.mb_email.value)) {
                    var msg = reg_mb_email_check();
                    if (msg) {
                        alert(msg);
                        jQuery(".email_msg").html(msg);
                        f.reg_mb_email.select();
                        return false;
                    }
                }

                document.getElementById("btn_submit").disabled = "disabled";

                return true;
            }

            function flogin_submit(f)
            {
                var mb_id = $.trim($(f).find("input[name=mb_id]").val()),
                    mb_password = $.trim($(f).find("input[name=mb_password]").val());

                if(!mb_id || !mb_password){
                    return false;
                }

                return true;
            }

            jQuery(function($){
                if( jQuery(".toggle .toggle-title").hasClass('active') ){
                    jQuery(".toggle .toggle-title.active").closest('.toggle').find('.toggle-inner').show();
                }
                jQuery(".toggle .toggle-title .right_i").click(function(){

                    var $parent = $(this).parent();

                    if( $parent.hasClass('active') ){
                        $parent.removeClass("active").closest('.toggle').find('.toggle-inner').slideUp(200);
                    } else {
                        $parent.addClass("active").closest('.toggle').find('.toggle-inner').slideDown(200);
                    }
                });
                // 모두선택
                $("input[name=chk_all]").click(function() {
                    if ($(this).prop('checked')) {
                        $("input[name^=agree]").prop('checked', true);
                    } else {
                        $("input[name^=agree]").prop("checked", false);
                    }
                });
            });
        </script>

    </div>
</div>

<!-- } 회원정보 입력/수정 끝 -->