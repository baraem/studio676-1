<?php
$sub_menu = "100930";
include_once('./_common.php');

if ($is_admin != 'super')
    alert('최고관리자만 접근 가능합니다.');

// 메뉴테이블 생성
if( !isset($g5['menu_table']) ){
    die('<meta charset="utf-8">dbconfig.php 파일에 <strong>$g5[\'menu_table\'] = G5_TABLE_PREFIX.\'menu\';</strong> 를 추가해 주세요.');
}


$g5['title'] = "메인이미지등록";
include_once('./admin.head.php');

$colspan = 7;

$sql = "SELECT `order` FROM main_image_view";
$order_result = sql_query($sql);
$order_list = array();
while($row=sql_fetch_array($order_result))
{
    $order_list[] = $row;
}

$orderArr = array();

for($i=0;$i<count($order_list); $i++)
{
    $orderArr[$i] = $order_list[$i]['order'];
}



/*echo "<pre>";
print_r($orderArr);
echo "</pre>";*/

$idx = $_GET['idx'];

if($idx != "undefined")
{
    $sql = "SELECT * FROM main_image_view WHERE idx = {$idx}";
    $result = sql_query($sql);
    $info = sql_fetch_array($result);

    $key = array_search($info['order'], $orderArr);
    array_splice($orderArr, $key, 1);
}


$orderArr = json_encode($orderArr);


?>


<form name="fmenulist" id="fmenulist" method="post" action="./main_image_upload_action.php" onsubmit="return main_image_update(this);" enctype="multipart/form-data">
    <input type="hidden" name="token" value="">
    <input type="hidden" name="idx" value="<?=$idx != "undefined" ? $idx : ""?>">

    <div id="menulist" class="tbl_head01 tbl_wrap">
        <table>
            <caption><?php echo $g5['title']; ?> 목록</caption>
            <thead>
                <tr>
                    <th scope="col">제목</th>
                    <td><input type="text" name="title" value="<?=$info['title']?>" placeholder="제목을 입력해주세요"></td>
                </tr>
                <tr>
                    <th scope="col">노출순서</th>
                    <td><input type="number" name="order" value="<?=$info['order']?>" placeholder="0"></td>
                </tr>
                <tr>
                    <th scope="col">연결링크</th>
                    <td>
                        <input type="text" name="link" value="<?=$info['link']?>" placeholder="">
                        <p>http:// 제외하고 입력해주세요</p>
                    </td>
                </tr>
                <tr>
                    <th scope="col">이미지</th>
                    <td>
                        <div class="file-wrap">
                            <input type="text" name="file-name" value="<?=$info['name']?>" readonly <?=$idx != "undefined" ? "disabled" : ""?>>
                            <input type="file" name="file" id="file" onchange="customFile(this)" value="<?=$info['path']?>/<?=$info['hashName']?>" placeholder="0" style="display:none;" <?=$idx != "undefined" ? "disabled" : ""?>>
                            <label for="file" class="file-wrap__label"><?=$idx != "undefined" ? "" : "찾아보기"?></label>
                        </div>
                    </td>
                </tr>
            </thead>
        </table>
    </div>

    <script>
        function customFile(t)
        {
            var $target = $(t);
            var input = $("input[name='file-name']");

            input.val($target[0].value);
        }
    </script>

    <style>
        #menulist input {
            width: 500px;
            height: 35px;
        }
        #menulist .file-wrap {
            font-size: 0;
        }
        #menulist input[type='number'] {
            width: 50px;
        }
        #menulist input[name='file-name'] {
            font-size: 12px;
            border: 1px solid #b7b7b7;
        }
        #menulist label[for='file'] {
            font-size: 12px;
            width: 80px;
            height: 35px;
            line-height: 35px;
            color: #252525;
            text-align:center;
            background-color: #ececec;
            border: 1px solid #b7b7b7;
            border-left: none;
            display: inline-block;
        }
        #menulist thead tr th {
            width: 200px;
            height: 56px;
            font-weight: bold;
            color: #000;
            text-align: left;
            border: none;
            background-color: #f6f6f6;
        }
        #menulist thead tr td {
            padding-left: 20px;
            width: 80%;
        }
    </style>

    <div class="btn_fixed_top">
        <button type="button" class="btn-imgPage btn_02" onclick="history.back()"><strong>목록</strong></button>
        <button type="submit" class="btn-imgPage action_btn"><strong>확인</strong><span class="sound_only"> 새창</span></button>
    </div>

</form>

<style>
    .btn-imgPage {
        width: 45px;
        height: 30px;
        border: none;
        border-radius: 5px;
    }
    .action_btn {
        color: #fff;
        background-color: #ff4081;
        border: none;
        border-radius: 5px;
    }
</style>

<script>
    $(function() {
        $(document).on("click", ".btn_add_submenu", function() {
            var ck = $(this).find($('input[type=hidden]')).eq(0).val();
            var me = $(this).find($('input[type=hidden]')).eq(1).val();
            var nm = $(this).find($('input[type=hidden]')).eq(2).val();
            if( ck == 'yes' )
            {
                window.open('<?=G5_THEME_URL?>/menuadd.php?item=' + me +'&nm=' + nm , 'name','width=430,height=500');
            }
            else
            {
                var code = $(this).closest("tr").find("input[name='code[]']").val().substr(0, 2);
                add_submenu(code);
            }
        });
    });

    function main_image_update(f)
    {

        var idx = "<?=$idx?>";

        if(f.title.value == "")
        {
            alert("제목을 입력해주세요");
            return false;
        }
        if(f.order.value == "")
        {
            alert("노출 순서를 입력해주세요");
            return false;
        }

        if(idx == "undefined") {
            if(f.file-name.value == "")
            {
                alert("이미지첨부 해주세요.");
                return false;
            }
        }


        var orderList = <?=$orderArr?>;

        if(orderList.indexOf(f.order.value) != -1) {
            alert("노출순서가 중복됩니다.");
            return false;
        }

    }

</script>

<?php
include_once ('./admin.tail.php');
?>
