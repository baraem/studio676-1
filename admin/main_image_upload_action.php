<?php
$sub_menu = "100930";
include_once('./_common.php');

if ($is_admin != 'super')
    alert('최고관리자만 접근 가능합니다.');

// 메뉴테이블 생성
if( !isset($g5['menu_table']) ){
    die('<meta charset="utf-8">dbconfig.php 파일에 <strong>$g5[\'menu_table\'] = G5_TABLE_PREFIX.\'menu\';</strong> 를 추가해 주세요.');
}


$g5['title'] = "메인이미지등록";
include_once('./admin.head.php');


if($_POST['idx'] == "")
{

    if(isset($_FILES['file']) && $_FILES['file']['name'] != "") {

        $file = $_FILES['file'];
        $today = date("YmdH");
        $upload_directory = "mainUploadDir/{$today}/";

        if(!is_dir($upload_directory))
        {
            @mkdir($upload_directory, 0777);
            @chmod($upload_directory, 0777);
        }

        $ext_str = "jpg,JPG,jpeg,JPEG,gif,GIF,png,PNG,bmp,BMP";
        $allowed_extensions = explode(',', $ext_str);
        $max_file_size = 5242880;
        $ext = substr($file['name'], strrpos($file['name'], '.') + 1);
        // 확장자 체크
        if(!in_array($ext, $allowed_extensions)) {
            echo "업로드할 수 없는 확장자 입니다.";
        }

        // 파일 크기 체크
        if($file['size'] >= $max_file_size) {
            echo "5MB 까지만 업로드 가능합니다.";
        }
        $hashName = md5(microtime());
        $path = $hashName . '.' . $ext;

        if(move_uploaded_file($file['tmp_name'], $upload_directory.$path)) {

            $sql = "INSERT INTO main_image_view SET 
                                              `title`    = '{$_POST['title']}',
                                              `name`     = '{$file['name']}',
                                              `hashName` = '{$path}',
                                              `link`     = '{$_POST['link']}',
                                              `order`    = '{$_POST['order']}',
                                              `path`     = '{$upload_directory}'
                                              
";
            if(sql_query($sql))
            {
                echo"<script>alert('성공'); location.href='/admin/main_image.php'</script>";
            }
            else
            {
                echo"<script>alert('파일업로드성공, 디비실패'); location.href='/admin/main_image.php'</script>";
            }

        }
    } else {
        echo "<script>alert('실패했습니다'); location.href='/admin/main_image.php'</script>";
    }

}else {
    $sql = "UPDATE main_image_view SET 
                              `title`    = '{$_POST['title']}',
                              `link`     = '{$_POST['link']}',
                              `order`    = '{$_POST['order']}'
                                    WHERE    
                                `idx`    = '{$_POST['idx']}' 
";

    if(sql_query($sql))
    {
        echo"<script>alert('성공'); location.href='/admin/main_image.php'</script>";
    }
    else
    {
        echo"<script>alert('실패'); location.href='/admin/main_image.php'</script>";
    }
}





include_once ('./admin.tail.php');
?>
