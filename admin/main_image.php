<?php
$sub_menu = "100930";
include_once('./_common.php');

if ($is_admin != 'super')
    alert('최고관리자만 접근 가능합니다.');

// 메뉴테이블 생성
if( !isset($g5['menu_table']) ){
    die('<meta charset="utf-8">dbconfig.php 파일에 <strong>$g5[\'menu_table\'] = G5_TABLE_PREFIX.\'menu\';</strong> 를 추가해 주세요.');
}


$g5['title'] = "메인이미지등록";
include_once('./admin.head.php');

$colspan = 4;

$sql = "SELECT * FROM main_image_view;";
$result = sql_query($sql);
$list = array();
while($row = sql_fetch_array($result))
{
    $list[] = $row;
}

?>
<form name="fmenulist" id="fmenulist" method="post" action="./menu_list_update.php" onsubmit="return fmenulist_submit(this);">
    <input type="hidden" name="token" value="">

    <div id="menulist" class="tbl_head01 tbl_wrap">
        <table>
            <thead>
            <tr>
                <th scope="col">번호</th>
                <th scope="col">제목</th>
                <th scope="col">노출순서</th>
                <th scope="col">삭제</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(count($list) != 0) {
                for ($i = 0; $i < count($list); $i++) { ?>
                    <tr>
                        <td width="3%"><?= $list[$i]['idx'] ?></td>
                        <td class="table__td--modifier"><?= $list[$i]['title'] ?></td>
                        <td width="5%"><?= $list[$i]['order'] ?></td>
                        <td width="8%">
                            <button type="button" class="table__btn--modify" onclick="mainImageUpdate(<?=$list[$i]['idx']?>)">수정</button>
                            <button type="button" class="table__btn--del" onclick="mainImageDelete(<?=$list[$i]['idx']?>)">삭제</button>
                        </td>
                    </tr>
                <?php }
            }else { ?>
                <tr id="empty_menu_list"><td colspan="<?=$colspan?>" class="empty_table">자료가 없습니다.</td></tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <div class="btn_fixed_top">
        <button type="button" onclick="mainImageUpdate()" class="action_btn"><strong>메인이미지 추가</strong><span class="sound_only"> 메인이미지</span></button>
    </div>

    <script>
        function mainImageUpdate(idx)
        {
            location.href = "main_image_upload.php?idx="+idx;
        }
        function mainImageDelete(idx)
        {
            if(!confirm("삭제하시겠습니까?")) return false;
            $.ajax({
                url: './main_image_delete.php',
                type: 'POST',
                data: 'idx='+idx,
                dataType : 'json',
                success: function(data)
                {
                    console.log(data);
                    if(data['res'] == "1") {
                        alert("삭제되었습니다");
                        location.reload();
                    }else {
                        alert("실패했습니다. 다시 시도해주세요")
                    }
                }
            });
        }
    </script>

</form>

<style>
    .action_btn {
        width: 98px;
        height: 30px;
        color: #fff;
        background-color: #ff4081;
        border: none;
        border-radius: 5px;
    }
</style>

<script>
    $(function() {
        $(document).on("click", ".btn_add_submenu", function() {
            var ck = $(this).find($('input[type=hidden]')).eq(0).val();
            var me = $(this).find($('input[type=hidden]')).eq(1).val();
            var nm = $(this).find($('input[type=hidden]')).eq(2).val();
            if( ck == 'yes' )
            {
                window.open('<?=G5_THEME_URL?>/menuadd.php?item=' + me +'&nm=' + nm , 'name','width=430,height=500');
            }
            else
            {
                var code = $(this).closest("tr").find("input[name='code[]']").val().substr(0, 2);
                add_submenu(code);
            }
        });

        $(document).on("click", ".btn_del_menu", function() {
            if(!confirm("메뉴를 삭제하시겠습니까?"))
                return false;

            if( $(this).find($('input[type=hidden]')).eq(0).val().length == 6 )
            {
                var item = new Object();
                item[0] = $(this).find($('input[type=hidden]')).eq(1).val();


                console.log(item);
                $.post( 'http://center.baraem.net/Core/theme/madang/ajax/loDelAjax.php', item, function(response){
                    var data = response;

                    if( data == 'true' )
                    {
                        alert('삭제하였습니다.');
                        location.reload();
                    }
                    else
                    {
                        alert('실패하였습니다.');
                        return false;
                    }
                })

            }

                var $tr = $(this).closest("tr");
                if($tr.find("td.sub_menu_class").size() > 0) {
                    $tr.remove();
                } else {
                    var code = $(this).closest("tr").find("input[name='code[]']").val().substr(0, 2);
                    $("tr.menu_group_"+code).remove();
                }

                if($("#menulist tr.menu_list").size() < 1) {
                    var list = "<tr id=\"empty_menu_list\"><td colspan='4' class=\"empty_table\">자료가 없습니다.</td></tr>\n";
                    $("#menulist table tbody").append(list);
                } else {
                    $("#menulist tr.menu_list").each(function(index) {
                        $(this).removeClass("bg0 bg1")
                            .addClass("bg"+(index % 2));
                    });
                }




        });
    });

    function add_menu()
    {
        var max_code = base_convert(0, 10, 36);
        $("#menulist tr.menu_list").each(function() {
            var me_code = $(this).find("input[name='code[]']").val().substr(0, 2);
            if(max_code < me_code)
                max_code = me_code;
        });

        var url = "./menu_form.php?code="+max_code+"&new=new";
        window.open(url, "add_menu", "left=100,top=100,width=550,height=650,scrollbars=yes,resizable=yes");
        return false;
    }

    function add_submenu(code)
    {
        var url = "./menu_form.php?code="+code;
        window.open(url, "add_menu", "left=100,top=100,width=550,height=650,scrollbars=yes,resizable=yes");
        return false;
    }

    function base_convert(number, frombase, tobase) {
        //  discuss at: http://phpjs.org/functions/base_convert/
        // original by: Philippe Baumann
        // improved by: Rafał Kukawski (http://blog.kukawski.pl)
        //   example 1: base_convert('A37334', 16, 2);
        //   returns 1: '101000110111001100110100'

        return parseInt(number + '', frombase | 0)
            .toString(tobase | 0);
    }

    function fmenulist_submit(f)
    {
        return true;
    }
</script>

<?php
include_once ('./admin.tail.php');
?>
