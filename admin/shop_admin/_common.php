<?php
define('G5_IS_ADMIN', true);
define('G5_IS_SHOP_ADMIN_PAGE', true);
include_once ('../../common.php');

if (!defined('G5_USE_SHOP') || !G5_USE_SHOP)
    die('<p>쇼핑몰 설치 후 이용해 주십시오.</p>');

include_once(G5_ADMIN_PATH.'/admin.lib.php');
include_once('./admin.shop.lib.php');

check_order_inicis_tmps();

// 8레벨 이하는 쇼핑몰 메뉴를 가린다
if(is_numeric($sub_menu) <= 400000 && is_numeric($sub_menu) >= 499999 && $member['mb_level'] < 9)
{
    exit;
}

?>
