<?php

include_once('./_common.php');

if ($is_admin != 'super')
    alert('최고관리자만 접근 가능합니다.');

if($_POST['idx']) {
    $sql = "SELECT * FROM main_image_view WHERE idx = {$_POST['idx']}";
    $result = sql_query($sql);
    $row = sql_fetch_array($result);

    unlink("./{$row['path']}{$row['hashName']}");


    $sql = "DELETE FROM main_image_view WHERE idx = {$_POST['idx']}";
    if(sql_query($sql)) {
        $res = array("res" => 1);
        echo json_encode($res);
    }
    else {
        $res = array("res" => 1);
        echo json_encode($res);
    }
}