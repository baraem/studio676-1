<?php
include_once('./_common.php');

$memberArr = array('baraem');

if(in_array($member['mb_id'], $memberArr))
{
    @include_once($member_skin_path.'/login_check.skin.php');

    $mb = get_member($_GET['userid']);
    // 회원아이디 세션 생성
    set_session('ss_mb_id', $mb['mb_id']);
    // FLASH XSS 공격에 대응하기 위하여 회원의 고유키를 생성해 놓는다. 관리자에서 검사함 - 110106
    set_session('ss_mb_key', md5($mb['mb_datetime'] . get_real_client_ip() . $_SERVER['HTTP_USER_AGENT']));
    goto_url(G5_URL);
}