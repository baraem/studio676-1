<?php
/**
 * Created by PhpStorm.
 * User: BARAEM
 * Date: 2018-07-31
 * Time: 오후 5:16
 * 이영진 작업 : sms 내용 설정과 관련된 작업임.
 */

include_once ( $_SERVER['DOCUMENT_ROOT'].'/common.php' );

//쇼핑몰 설정 처음 들어갔을 경우 회원가입시 문자를 보여준다.
if( $_POST['type'] == 'sms_selStart' )
{
    $sql = "
        SELECT
            *
         FROM
            sms_msg
         WHERE
            msg_key = '1'   
    ";

    $result = sql_query($sql);
    while ( $row = $result -> fetch_array() )
    {
        $list[] = $row;
    }

    echo json_encode($list);
}

//문자 설정을 선택 하였을 시
if( $_POST['type'] == 'msg_sel' )
{
    $sql = "
        SELECT
            *
         FROM
            sms_msg
         WHERE
            msg_key = '{$_POST['msg_key']}'   
    ";

    $result = sql_query($sql);
    while ( $row = $result -> fetch_array() )
    {
        $list[] = $row;
    }

    echo json_encode($list);
}

//문자 저장하는 경우
if( $_POST['type'] == 'new' )
{
    $result = 'true';
    $sql = "
        UPDATE sms_msg 
         SET
            msg_val = '{$_POST['msg_val']}'
         WHERE
            msg_key = '{$_POST['msg_key']}'
    ";

    if( !sql_query($sql) ) $result = 'false';

    echo $result;
}