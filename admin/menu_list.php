<?php
$sub_menu = "100290";
include_once('./_common.php');

if ($is_admin != 'super')
    alert('최고관리자만 접근 가능합니다.');

// 메뉴테이블 생성
if( !isset($g5['menu_table']) ){
    die('<meta charset="utf-8">dbconfig.php 파일에 <strong>$g5[\'menu_table\'] = G5_TABLE_PREFIX.\'menu\';</strong> 를 추가해 주세요.');
}

if(!sql_query(" DESCRIBE {$g5['menu_table']} ", false)) {
    sql_query(" CREATE TABLE IF NOT EXISTS `{$g5['menu_table']}` (
                  `me_id` int(11) NOT NULL AUTO_INCREMENT,
                  `me_code` varchar(255) NOT NULL DEFAULT '',
                  `me_name` varchar(255) NOT NULL DEFAULT '',
                  `me_link` varchar(255) NOT NULL DEFAULT '',
                  `me_target` varchar(255) NOT NULL DEFAULT '0',
                  `me_order` int(11) NOT NULL DEFAULT '0',
                  `me_use` tinyint(4) NOT NULL DEFAULT '0',
                  `me_mobile_use` tinyint(4) NOT NULL DEFAULT '0',
                  PRIMARY KEY (`me_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 ", true);
}

$sql = " select * from {$g5['menu_table']} order by me_id ";
$result = sql_query($sql);

$g5['title'] = "메뉴설정";
include_once('./admin.head.php');

$colspan = 7;
?>

<style>
    .sub_me_name{
        width: 60px;
        height: 27px;
        margin: 3px 0 0 2px;
    }

    .sub_me_link{
        width: 100%;
        height: 27px;
        margin: 3px 0 0 2px;
    }

    .sub_me_target{
        margin: 3px 0 0 2px;
    }

    .sub_me_order{
        width: 52px;
        height: 27px;
        margin: 3px 0 0 2px;
    }

    .sub_me_use, .sub_me_mobile_use{
        margin: 3px 0 0 2px;
    }

    .sub_me_3dep{
        margin: 3px 0 0 2px;
        height: 27px;
    }
</style>

<div class="local_desc01 local_desc">
    <p><strong>주의!</strong> 메뉴설정 작업 후 반드시 <strong>확인</strong>을 누르셔야 저장됩니다.</p>
    <p>3단계메뉴를 사용하기 위해서는 <strong>3단계 메뉴</strong>항목을 <strong>사용함</strong>으로 바꾸시고 저장한 다음에 사용하세요</p>

</div>

<form name="fmenulist" id="fmenulist" method="post" action="./menu_list_update.php" onsubmit="return fmenulist_submit(this);">
    <input type="hidden" name="token" value="">

    <div id="menulist" class="tbl_head01 tbl_wrap">
        <table>
            <caption><?php echo $g5['title']; ?> 목록</caption>
            <thead>
            <tr>
                <th scope="col">메뉴</th>
                <th scope="col">링크</th>
                <th scope="col">새창</th>
                <th scope="col">순서</th>
                <th scope="col">PC사용</th>
                <th scope="col">모바일사용</th>
                <th scope="col">3단계 메뉴</th>
                <th scope="col">관리</th>
            </tr>
            </thead>
            <tbody>
            <?php

            $x = 0;
            for ($i=0; $row=sql_fetch_array($result); $i++)
            {
                ++$x;
                $bg = 'bg'.($i%2);
                $sub_menu_class = '';
                if(strlen($row['me_code']) == 4) {
                    $sub_menu_class = ' sub_menu_class';
                    $sub_menu_info = '<span class="sound_only">'.$row['me_name'].'의 서브</span>';
                    $sub_menu_ico = '<span class="sub_menu_ico"></span>';
                    $item = $row['me_code'];
                }

                $search  = array('"', "'");
                $replace = array('&#034;', '&#039;');
                $me_name = str_replace($search, $replace, $row['me_name']);
                ?>
                <tr class="<?php echo $bg; ?> menu_list menu_group_<?php echo substr($row['me_code'], 0, 2); ?>">
                    <td class="td_category<?php echo $sub_menu_class; ?>">
                        <input type="hidden" name="code[]" value="<?php echo substr($row['me_code'], 0, 2) ?>">
                        <label for="me_name_<?php echo $i; ?>" class="sound_only"><?php echo $sub_menu_info; ?> 메뉴<strong class="sound_only"> 필수</strong></label>
                        <input type="text" name="me_name[]" value="<?php echo $me_name; ?>" id="me_name_<?php echo $i; ?>" required class="required tbl_input full_input">

                        <?
                        $item1 = substr($row['me_code'],0,1);
                        ?>
                        <input type="hidden" name="gr_id[]" value="menu0<?=$item1?>"/>
                        <?

                        if( $row['me_3dep'] == 'yes' )
                        {
                            $itemSql = "
                        SELECT *
                         FROM
                            BRI_menu_sub
                         WHERE
                            me_code LIKE '{$row['me_code']}%'
                         ORDER BY idx ASC   
                    ";

                            $itemResult = sql_query($itemSql);
                            while( $itemRow = $itemResult -> fetch_array() )
                            {
                                $list[] = $itemRow;
                                $codechk[] = substr($itemRow['me_code'],0,4);
                            }
                            $i = -1;
                            foreach ( $list as $k => $v )
                            {
                                $i++;
                                if( $row['me_code'] != $codechk[$i] )
                                {
                                    continue;
                                }
                                ?>
                                └<input type="text" name="sub_me_name[]" value="<?php echo $v['bo_subject']; ?>" id="sub_me_name_<?php echo $i; ?>" required class="sub_me_name"><br>
                                    <input type="hidden" name="h1[]" value="<?=$v['me_code']?>">
                            <?}
                        }
                        ?>
                    </td>
                    <td>
                        <label for="me_link_<?php echo $i; ?>" class="sound_only">링크<strong class="sound_only"> 필수</strong></label>
                        <input type="text" name="me_link[]" value="<?php echo $row['me_link'] ?>" id="me_link_<?php echo $i; ?>" required class="required tbl_input full_input">
                        <?
                        if( $row['me_3dep'] == 'yes' )
                        {
                            $i = -1;
                            foreach ( $list as $k => $v )
                            {
                                $i++;
                                if( $row['me_code'] != $codechk[$i] )
                                {
                                    continue;
                                }
                                ?>
                                <input type="text" name="sub_me_link[]" value="<?php echo G5_BBS_URL.'/border.php?bo_table='.$v['bo_table'] ?>" id="me_link_<?php echo $i; ?>" required class="sub_me_link"><br>
                            <?}
                        }
                        ?>
                    </td>
                    <td class="td_mng">
                        <label for="me_target_<?php echo $i; ?>" class="sound_only">새창</label>
                        <select name="me_target[]" id="me_target_<?php echo $i; ?>">
                            <option value="self"<?php echo get_selected($row['me_target'], 'self', true); ?>>사용안함</option>
                            <option value="blank"<?php echo get_selected($row['me_target'], 'blank', true); ?>>사용함</option>
                        </select>
                        <?
                        if( $row['me_3dep'] == 'yes' )
                        {
                            $i = -1;
                            foreach ( $list as $k => $v )
                            {
                                $i++;
                                if( $row['me_code'] != $codechk[$i] )
                                {
                                    continue;
                                }
                                ?>
                                <select name="sub_me_target[]" id="me_target_<?php echo $i; ?>" class="sub_me_target">
                                    <option value="self"<?php echo get_selected($v['me_target'], 'self', true); ?>>사용안함</option>
                                    <option value="blank"<?php echo get_selected($v['me_target'], 'blank', true); ?>>사용함</option>
                                </select>
                            <?}
                        }
                        ?>
                    </td>
                    <td class="td_num">
                        <label for="me_order_<?php echo $i; ?>" class="sound_only">순서</label>
                        <input type="text" name="me_order[]" value="<?php echo $row['me_order'] ?>" id="me_order_<?php echo $i; ?>" class="tbl_input" size="5">
                        <?
                        if( $row['me_3dep'] == 'yes' )
                        {
                            $i = -1;
                            foreach ( $list as $k => $v )
                            {
                                $i++;
                                if( $row['me_code'] != $codechk[$i] )
                                {
                                    continue;
                                }
                                ?>
                                <input type="text" name="sub_me_order[]" value="<?php echo $v['me_order'] ?>" id="me_order_<?php echo $i; ?>" class="sub_me_order" size="5">
                            <?}
                        }
                        ?>
                    </td>
                    <td class="td_mng">
                        <label for="me_use_<?php echo $i; ?>" class="sound_only">PC사용</label>
                        <select name="me_use[]" id="me_use_<?php echo $i; ?>">
                            <option value="1"<?php echo get_selected($row['me_use'], '1', true); ?>>사용함</option>
                            <option value="0"<?php echo get_selected($row['me_use'], '0', true); ?>>사용안함</option>
                        </select>
                        <?
                        if( $row['me_3dep'] == 'yes' )
                        {
                            $i = -1;
                            foreach ( $list as $k => $v )
                            {
                                $i++;
                                if( $row['me_code'] != $codechk[$i] )
                                {
                                    continue;
                                }
                                ?>
                                <select name="sub_me_use[]" id="me_use_<?php echo $i; ?>" class="sub_me_use">
                                    <option value="1"<?php echo get_selected($v['me_use'], '1', true); ?>>사용함</option>
                                    <option value="0"<?php echo get_selected($v['me_use'], '0', true); ?>>사용안함</option>
                                </select>
                            <?}
                        }
                        ?>
                    </td>
                    <td class="td_mng">
                        <label for="me_mobile_use_<?php echo $i; ?>" class="sound_only">모바일사용</label>
                        <select name="me_mobile_use[]" id="me_mobile_use_<?php echo $i; ?>">
                            <option value="1"<?php echo get_selected($row['me_mobile_use'], '1', true); ?>>사용함</option>
                            <option value="0"<?php echo get_selected($row['me_mobile_use'], '0', true); ?>>사용안함</option>
                        </select>
                        <?
                        if( $row['me_3dep'] == 'yes' )
                        {
                            $i = -1;
                            foreach ( $list as $k => $v )
                            {
                                $i++;
                                if( $row['me_code'] != $codechk[$i] )
                                {
                                    continue;
                                }
                                ?>
                                <select name="sub_me_mobile_use[]" id="me_mobile_use_<?php echo $i; ?>" class="sub_me_mobile_use">
                                    <option value="1"<?php echo get_selected($v['me_mobile_use'], '1', true); ?>>사용함</option>
                                    <option value="0"<?php echo get_selected($v['me_mobile_use'], '0', true); ?>>사용안함</option>
                                </select>
                            <?}
                        }
                        ?>
                    </td>

                    <!--추가소스 관리설정.-->

                    <td class="td_mng">
                        <label for="me_3dep_<?php echo $i; ?>" class="sound_only">단계</label>
                        <select name="me_3dep[]" id="me_3dep_<?php echo $i; ?>">
                            <option value="no" <?=$row['me_3dep'] == 'no' ? 'selected': ''?>>사용안함</option>
                            <option value="yes" <?=$row['me_3dep'] == 'yes' ? 'selected':''?>>사용함</option>
                        </select>
                        <?
                        if( $row['me_3dep'] == 'yes' )
                        {
                            $i = -1;
                            foreach ( $list as $k => $v )
                            {
                                $i++;
                                if( $row['me_code'] != $codechk[$i] )
                                {
                                    continue;
                                }
                                ?>
                                <p class="sub_me_3dep">3뎁스메뉴</p>
                            <?}
                        }
                        ?>
                    </td>
                    <td class="td_mng">
                        <!--이부분 변경시 메뉴설정 관리 부분에추가 삭제 여부 변경.-->
                        <?php if(strlen($row['me_code']) == 2 || strlen($row['me_code']) < 5) { ?>
                            <button type="button" class="btn_add_submenu btn_03 ">추가
                                <input type="hidden" name="ck"  value="<?=$row['me_3dep']?>"/>
                                <input type="hidden" name="ck"  value="<?=$row['me_code']?>"/>
                                <input type="hidden" name="lo_sub3" value="<?=$row['me_name']?>"/>
                            </button>
                        <?php } ?>
                        <button type="button" class="btn_del_menu btn_02">삭제
                            <input type="hidden" name="lo_sub" value="<?=$row['me_code']?>"
                        </button>
                        <?
                        if( $row['me_3dep'] == 'yes' )
                        {
                            $i = -1;
                            foreach ( $list as $k => $v )
                            {
                                $i++;
                                if( $row['me_code'] != $codechk[$i] )
                                {
                                    continue;
                                }
                                ?>
                                <button type="button" class="btn_del_menu btn_02" class="btndel">삭제
                                    <input type="hidden" name="lo_sub" value="<?=$v['me_code']?>"/>
                                    <input type="hidden" name="lo_sub2" value="<?=$v['idx']?>"/>
                                </button>
                                <br>
                            <?}
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }

            if ($i==0)
                echo '<tr id="empty_menu_list"><td colspan="'.$colspan.'" class="empty_table">자료가 없습니다.</td></tr>';
            ?>
            </tbody>
        </table>
    </div>

    <div class="btn_fixed_top">
        <button type="button" onclick="return add_menu();" class="btn btn_02">메뉴추가<span class="sound_only"> 새창</span></button>
        <input type="submit" name="act_button" value="확인" class="btn_submit btn ">
    </div>

</form>

<script>
    $(function() {
        $(document).on("click", ".btn_add_submenu", function() {
            var ck = $(this).find($('input[type=hidden]')).eq(0).val();
            var me = $(this).find($('input[type=hidden]')).eq(1).val();
            var nm = $(this).find($('input[type=hidden]')).eq(2).val();
            if( ck == 'yes' )
            {
                window.open('<?=G5_THEME_URL?>/menuadd.php?item=' + me +'&nm=' + nm , 'name','width=430,height=500');
            }
            else
            {
                var code = $(this).closest("tr").find("input[name='code[]']").val().substr(0, 2);
                add_submenu(code);
            }
        });

        $(document).on("click", ".btn_del_menu", function() {
            if(!confirm("메뉴를 삭제하시겠습니까?"))
                return false;

            if( $(this).find($('input[type=hidden]')).eq(0).val().length == 6 )
            {
                var item = new Object();
                item[0] = $(this).find($('input[type=hidden]')).eq(1).val();


                console.log(item);
                $.post( 'http://center.baraem.net/Core/theme/madang/ajax/loDelAjax.php', item, function(response){
                    var data = response;

                    if( data == 'true' )
                    {
                        alert('삭제하였습니다.');
                        location.reload();
                    }
                    else
                    {
                        alert('실패하였습니다.');
                        return false;
                    }
                })

            }

                var $tr = $(this).closest("tr");
                if($tr.find("td.sub_menu_class").size() > 0) {
                    $tr.remove();
                } else {
                    var code = $(this).closest("tr").find("input[name='code[]']").val().substr(0, 2);
                    $("tr.menu_group_"+code).remove();
                }

                if($("#menulist tr.menu_list").size() < 1) {
                    var list = "<tr id=\"empty_menu_list\"><td colspan=\"<?php echo $colspan; ?>\" class=\"empty_table\">자료가 없습니다.</td></tr>\n";
                    $("#menulist table tbody").append(list);
                } else {
                    $("#menulist tr.menu_list").each(function(index) {
                        $(this).removeClass("bg0 bg1")
                            .addClass("bg"+(index % 2));
                    });
                }




        });
    });

    function add_menu()
    {
        var max_code = base_convert(0, 10, 36);
        $("#menulist tr.menu_list").each(function() {
            var me_code = $(this).find("input[name='code[]']").val().substr(0, 2);
            if(max_code < me_code)
                max_code = me_code;
        });

        var url = "./menu_form.php?code="+max_code+"&new=new";
        window.open(url, "add_menu", "left=100,top=100,width=550,height=650,scrollbars=yes,resizable=yes");
        return false;
    }

    function add_submenu(code)
    {
        var url = "./menu_form.php?code="+code;
        window.open(url, "add_menu", "left=100,top=100,width=550,height=650,scrollbars=yes,resizable=yes");
        return false;
    }

    function base_convert(number, frombase, tobase) {
        //  discuss at: http://phpjs.org/functions/base_convert/
        // original by: Philippe Baumann
        // improved by: Rafał Kukawski (http://blog.kukawski.pl)
        //   example 1: base_convert('A37334', 16, 2);
        //   returns 1: '101000110111001100110100'

        return parseInt(number + '', frombase | 0)
            .toString(tobase | 0);
    }

    function fmenulist_submit(f)
    {
        return true;
    }
</script>

<?php
include_once ('./admin.tail.php');
?>
