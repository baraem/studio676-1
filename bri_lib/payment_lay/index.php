<?php
//결제시 안내사항
//include_once(G5_PATH.'/bri_lib/payment_lay/index.php');
?>

<div class="payment_lay">
    <div class="lay_box">
        <div class="lay_box2">
            <p class="p_10">
                <span class="color_red">*</span> 결제시 안내사항
            </p>
            <hr class="hr_10"/>
            <p>‘주문하기’ 버튼 클릭후 카드결제 모듈이 뜰 경우 <span class="color_red">바로 결제</span>를 진행해주셔야 합니다</p>
            <p class="color_red">(대기시간이 오래 걸릴 수록 주문접수가 완료되지 않는 현상이 발생할 수 있습니다.)</p>
            <hr class="hr_20"/>
            <p>결제완료 후 최종적으로 <span class="color_red">완료페이지가 뜰 때까지 창을 닫으시면 안됩니다.</span></p>
            <p>(주문이 최종적으로 접수되지 않을 가능성이 있으니 완료페이지가 뜰떄까지 대기바랍니다.)</p>
        </div>
    </div>
</div>
<div class="payment_lay alert">
    <div class="lay_box">
        <div class="lay_box2">
            <p class="p_10">
                <span class="color_red">*</span> 결제시 안내사항
            </p>
            <hr class="hr_10"/>
            <p>‘주문하기’ 버튼 클릭후 카드결제 모듈이 뜰 경우 <span class="color_red">바로 결제</span>를 진행해주셔야 합니다</p>
            <p class="color_red">(대기시간이 오래 걸릴 수록 주문접수가 완료되지 않는 현상이 발생할 수 있습니다.)</p>
            <hr class="hr_20"/>
            <p>결제완료 후 최종적으로 <span class="color_red">완료페이지가 뜰 때까지 창을 닫으시면 안됩니다.</span></p>
            <p>(주문이 최종적으로 접수되지 않을 가능성이 있으니 완료페이지가 뜰떄까지 대기바랍니다.)</p>
        </div>
    </div>
</div>
<style>
    .hr_10{height: 10px; display: block;border: 0;}
    .hr_20{height: 20px; display: block;border: 0;}

    .payment_lay{background-color: #ffffff;display: block;padding: 10px 0 0;display: none;}
    .payment_lay .lay_box{background-color: #f4f4f4;padding: 6px;}

    .payment_lay.alert{position: fixed;top: 0; left: 0; width: 100%; height: 100%; z-index: 9999; background-color:rgba( 0, 0, 0, 0.6 );}
    .payment_lay.alert .lay_box{background-color: #f4f4f4;padding: 6px;position: absolute;top: 50%;left: 50%;width: 90%;max-width: 600px;
        -webkit-transform: translate(-50%,-50%);
        -moz-transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        -o-transform: translate(-50%,-50%);
        transform: translate(-50%,-50%);
    }

    .payment_lay .lay_box2{border: 1px solid #b7b7b7;padding: 24px;}
    .payment_lay .lay_box2 p{font-size: 14px;color: #363636;font-weight: 500;line-height: 21px;letter-spacing: -1px;margin: 0 !important;}
    .payment_lay .lay_box2 span{font-size: 14px;}
    .payment_lay .lay_box2 .p_10{font-size: 15px;font-weight: bold;}
    .payment_lay .lay_box2 .color_red{color: #ed1c24;}
</style>
<script>
    $('#od_settle_card').click(function (e) {
        $('.payment_lay').show();
    });

    $('.payment_lay.alert').click(function (e) {
        $(this).hide();
    });

    $("#od_settle_bank").on("click", function() {
        $('.payment_lay').hide();
    });
</script>